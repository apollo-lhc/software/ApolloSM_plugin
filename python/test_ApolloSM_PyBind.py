#
# Python script to test Python binding of ApolloSM class and
# show examples of how to use it in Python3 programs.
#

# Should be using Python >= 3.6
import sys
if sys.version_info < (3,6):
    raise Exception("Please use Python 3.6 or a more recent version.")

import ApolloSM

# Create the ApolloSM instance with the connections file
sm = ApolloSM.ApolloSM(["/opt/address_table/connections.xml"])

# Test a read
register = "PL_MEM.USERS_INFO.USERS.COUNT"
value = sm.ReadRegister(register)
print("Test read:")
print(f"{register}: {value}")

# Test a write
scratch_register = "PL_MEM.SCRATCH.WORD_00"
sm.WriteRegister(scratch_register, 1)
print(f"Write 1 to {scratch_register}")
print(f"Reading back: {sm.ReadRegister(scratch_register)}")

# Write 0 back
sm.WriteRegister(scratch_register, 0)
print(f"Write 0 to {scratch_register}")
print(f"Reading back: {sm.ReadRegister(scratch_register)}")

# Error handling examples: All BUException-derived exception types
# are ported to Python3 using pybind11
try:
    sm.ReadRegister("ABC")
except ApolloSM.BAD_REG_NAME as e:
    print(f"Caught BAD_REG_NAME, error message is: {str(e)}")

# Reading this register will cause a bus error if the first FPGA
# on the CM is not programmed.
try:
    sm.ReadRegister("F1_IPBUS.MEM")
except ApolloSM.BUS_ERROR as e:
    print(f"Caught BUS_ERROR, error message is: {str(e)}")