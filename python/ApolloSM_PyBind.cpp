/*
 * Python binding for the ApolloSM class using pybind11 library.
 */

#include <ApolloSM/ApolloSM.hh>

/* Headers for exception class definitions. */
#include <ApolloSM/ApolloSM_Exceptions.hh>
#include <BUTool/ToolException.hh>
#include <IPBusIO/IPBusExceptions.hh>
#include <IPBusRegHelper/IPBusRegHelperExceptions.hh>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

/*
 * The following defines the bindings for the IPBusConnection, IPBusIO
 * and ApolloSM classes.
 * The inheritance is set up such that ApolloSM class inherits from
 * the IPBusConnection and IPBusIO classes.
 */
PYBIND11_MODULE(ApolloSM, m)
{
    py::class_<IPBusConnection>(m, "IPBusConnection")
        .def(py::init<std::string const &, std::vector<std::string> const &>())
        .def("Connect", &IPBusConnection::Connect);

    py::class_<IPBusIO>(m, "IPBusIO")
        .def(py::init<std::shared_ptr<uhal::HwInterface>>())
        .def("ReadAddress", &IPBusIO::ReadAddress)
        .def("ReadRegister", &IPBusIO::ReadRegister)
        .def("GetRegsRegex", &IPBusIO::GetRegsRegex)
        .def("GetRegAddress", &IPBusIO::GetRegAddress)
        .def("GetRegMask", &IPBusIO::GetRegMask)
        .def("GetRegSize", &IPBusIO::GetRegSize)
        .def("GetRegPermissions", &IPBusIO::GetRegPermissions)
        .def("GetRegDescription", &IPBusIO::GetRegDescription)
        .def("GetRegDebug", &IPBusIO::GetRegDebug)
        .def("WriteAddress", &IPBusIO::WriteAddress)
        .def("WriteRegister", &IPBusIO::WriteRegister)
        .def("GetRegisterNamesFromTable", &IPBusIO::GetRegisterNamesFromTable, py::arg("tableName"), py::arg("statusLevel") = 1);

    py::class_<ApolloSM, IPBusConnection, IPBusIO>(m, "ApolloSM")
        .def(py::init<std::vector<std::string> const &>())
        .def("GenerateStatusDisplay", &ApolloSM::GenerateStatusDisplay)
        .def("GenerateHTMLStatus", &ApolloSM::GenerateHTMLStatus)
        .def("GenerateGraphiteStatus", &ApolloSM::GenerateGraphiteStatus)
        .def("UART_Terminal", &ApolloSM::UART_Terminal)
        .def("UART_CMD", &ApolloSM::UART_CMD, py::arg("ttyDev"), py::arg("sendline"), py::arg("promptChar") = '%')
        .def("svfplayer", &ApolloSM::svfplayer, py::arg("svfFile"), py::arg("XVCReg"), py::arg("displayProgress") = false)
        .def("progam_fpga", &ApolloSM::program_fpga, py::arg("svfFile"), py::arg("displayProgress") = false)
        .def("PowerUpCM", &ApolloSM::PowerUpCM, py::arg("CM_ID"), py::arg("wait") = -1)
        .def("PowerDownCM", &ApolloSM::PowerDownCM, py::arg("CM_ID"), py::arg("wait") = -1)
        .def("unblockAXI", &ApolloSM::unblockAXI, py::arg("name") = "")
        .def("DebugDump", &ApolloSM::DebugDump)
        .def("restartCMuC", &ApolloSM::restartCMuC)
        .def("GetSerialNumber", &ApolloSM::GetSerialNumber)
        .def("GetRevNumber", &ApolloSM::GetRevNumber)
        .def("GetShelfID", &ApolloSM::GetShelfID)
        .def("GetSlot", &ApolloSM::GetSlot)
        .def("GetZynqIP", &ApolloSM::GetZynqIP)
        .def("GetIPMCIP", &ApolloSM::GetIPMCIP);

    /*
     * Register exceptions to the Python bindings.
     */

    /* Exceptions defined within ApolloSM. */
    py::register_exception<BUException::IO_ERROR>(m, "IO_ERROR");
    py::register_exception<BUException::FILE_ERROR>(m, "FILE_ERROR");
    py::register_exception<BUException::JTAG_ERROR>(m, "JTAG_ERROR");
    py::register_exception<BUException::EYESCAN_ERROR>(m, "EYESCAN_ERROR");
    py::register_exception<BUException::BAD_PARAMETER>(m, "BAD_PARAMETER");
    py::register_exception<BUException::APOLLO_SM_BAD_VALUE>(m, "APOLLO_SM_BAD_VALUE");

    /* Exceptions defined within BUTool. */
    py::register_exception<BUException::COMMAND_LIST_ERROR>(m, "COMMAND_LIST_ERROR");
    py::register_exception<BUException::DEVICE_CREATION_ERROR>(m, "DEVICE_CREATION_ERROR");
    py::register_exception<BUException::CREATOR_UNREGISTERED>(m, "CREATOR_UNREGISTERED");
    py::register_exception<BUException::REG_READ_DENIED>(m, "REG_READ_DENIED");
    py::register_exception<BUException::REG_WRITE_DENIED>(m, "REG_WRITE_DENIED");
    py::register_exception<BUException::BAD_REG_NAME>(m, "BAD_REG_NAME");
    py::register_exception<BUException::BAD_VALUE>(m, "BAD_VALUE");
    py::register_exception<BUException::BAD_MARKUP_NAME>(m, "BAD_MARKUP_NAME");
    py::register_exception<BUException::BUS_ERROR>(m, "BUS_ERROR");
    py::register_exception<BUException::TEXTIO_BAD_INIT>(m, "TEXTIO_BAD_INIT");
    py::register_exception<BUException::FORMATTING_NOT_IMPLEMENTED>(m, "FORMATTING_NOT_IMPLEMENTED");
    py::register_exception<BUException::FUNCTION_NOT_IMPLEMENTED>(m, "FUNCTION_NOT_IMPLEMENTED");

    /* Exceptions defined within BUTool IPBus helpers. */
    py::register_exception<BUException::IPBUS_CONNECTION_ERROR>(m, "IPBUS_CONNECTION_ERROR");
    py::register_exception<BUException::BAD_STATE>(m, "BAD_STATE");
    py::register_exception<BUException::IPBUS_CONNECTION_ERROR>(m, "IPBUS_CONNECTION_ERROR");
}