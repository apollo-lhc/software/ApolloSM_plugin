#ifndef EYESCAN_BATCH_HH
#define EYESCAN_BATCH_HH

#include "ApolloSM/ApolloSM_Exceptions.hh"
#include <ApolloSM/ApolloSM.hh>
#include <ApolloSM/eyescan_class.hh>
#include <map>
#include <memory>
#include <string>
#include <vector>

class eyescan_batch {
private:
  struct scanInfo {
    eyescan local;
    eyescan global;
    int xIncr;
    int yIncr;
    int prescale;
    time_t start;
    bool link_up;
    scanInfo(eyescan _local, eyescan _global, int _xIncr, int _yIncr,
             int _prescale)
        : local{_local}, global{_global}, xIncr{_xIncr}, yIncr{_yIncr},
          prescale{_prescale} {
      start = std::time(nullptr);
      link_up = true; // ??
    }
  };

  std::shared_ptr<ApolloSM> SM;
  int defaultXIncr;
  int defaultYIncr;
  int defaultPrescale;
  std::string odir;

  std::map<std::string, scanInfo> scanMap;
  std::vector<BUException::EYESCAN_ERROR> exceptionMessages;

public:
  enum class cycle_t { UNINIT, RUNNING, ERR_RUNNING, DONE, ERR_DONE };
  enum class node_op_t { ERROR, OK, INVALID };

private:
  cycle_t cycleStatus = cycle_t::UNINIT;
  int completedScans = 0;

  std::vector<std::string> discoverNodes();

public:
  eyescan_batch(std::shared_ptr<ApolloSM> _SM, std::string const &outdir,
                int _xIncr, int yIncr, int _prescale);
  eyescan_batch() = delete;
  cycle_t update();
  void plotScans();
  std::vector<std::string> terminateRunningScans();
  node_op_t changePrescale(std::string const &node, int _prescale);
  node_op_t incrementPrescale(std::string const &node);
  node_op_t decrementPrescale(std::string const &node);
  int getPrescale(std::string const &node);
  node_op_t intersectNodes();
  node_op_t addNodes(std::vector<std::string> _nodes, int _xIncr = -1,
                     int _yIncr = -1, int _prescale = -1);
  void swapSM(std::shared_ptr<ApolloSM> _SM);
  void resetGlobalScans();
  double getOpenArea(std::string node);
  std::vector<std::string> linksUp();
  std::vector<BUException::EYESCAN_ERROR> fetchErrorMessages();
  std::vector<std::string> listBaseNodes();
};

#endif // !EYESCAN_BATCH_HH
