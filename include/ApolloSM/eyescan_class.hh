// clang-format off
#ifndef __EYESCANCLASS_HH__
#define __EYESCANCLASS_HH__
#include "RegisterHelper/RegisterHelperIO.hh"
#include <ApolloSM/ApolloSM.hh>
#include <queue>
#include <inttypes.h> //pri macros


// Correct eye scan attribute values
#define ES_EYE_SCAN_EN 0x1
#define ES_ERRDET_EN 0x1
#define PMA_CFG 0x000 // Actually 10 0s: 10b0000000000
//#define PMA_RSV2 0x1
//#define ES_QUALIFIER 0x0000
//#define ES_QUAL_MASK 0xFFFF

//#define RX_DATA_WIDTH_GTX 0x4 // zynq
//#define RX_INT_DATAWIDTH_GTX 0x1 // We use 32 bit
//
//#define RX_DATA_WIDTH_GTH 0x4 // kintex
//#define RX_INT_DATAWIDTH_GTH 0x0 //16 bit
//
//#define RX_DATA_WIDTH_GTY 0x6 // virtex
//#define RX_INT_DATAWIDTH_GTY 0x1 //32 bit
#define DFE 0
#define LPM 1

#define MAX_UI_MAG 0.5

#define MAX_PRESCALE_VALUE ((1<<5)-1)

#define MAX_Y_BIN_MAG ((1<<7)-1)

#define PRESCALE_START 0
#define PRESCALE_STEP 3


class eyescan
{
public:
  // SCAN_ERR was never properly implemented
  typedef enum { UNINIT, SCAN_INIT, SCAN_READY, SCAN_START, SCAN_PIXEL, SCAN_DONE, SCAN_KILL , SCAN_ERR } ES_state_t;
  typedef enum { FIRST, SECOND, LPM_MODE} DFE_state_t;
  enum class SERDES_t : uint8_t {UNKNOWN=0,GTH_USP=1,GTX_7S=2,GTY_USP=3,GTH_7S=4};
  enum class LINK_STATE_T { DOWN, UP };

private:
  // All necessary information to plot an eyescan
  struct eyescanCoords {
    eyescanCoords(){clear();};
    double   voltage;
    bool     voltageReal;
    double   phase;
    uint16_t horzWriteVal;
    uint16_t vertWriteVal;

    uint8_t  prescale;
    double   BER;
    uint64_t sample0;
    uint64_t error0;
    uint64_t sample1;
    uint64_t error1;    
    void clear(){
      // Clear all values
      voltage      = 0;
      voltageReal  = 0;
      phase        = 0;
      horzWriteVal = 0;
      vertWriteVal = 0;
      reset();
    };
    void reset(){
      // Reset the data output values
      BER         = 0;
      sample0     = 0;
      error0      = 0;
      sample1     = 0;
      error1      = 0;
      prescale = PRESCALE_START;
    };
    void print(uint8_t mask = 0xFF){
      if(mask & 0x01){fprintf(stderr,"Pixel @ %5.3e,%5.3e\n",phase,voltage);}
      if(mask & 0x02){fprintf(stderr,"  write: %5d, %5d\n",horzWriteVal,vertWriteVal);}
      if(mask & 0x04){fprintf(stderr,"  PS:  %u\n",prescale);}
      if(mask & 0x08){fprintf(stderr,"  BER: %e\n",BER);}
      if(mask & 0x10){fprintf(stderr,"  e0/s0:   %" PRIu64 "/%" PRIu64"\n",error0,sample0);}
      if(mask & 0x10){fprintf(stderr,"  e1/s1:   %" PRIu64 "/%" PRIu64"\n",error1,sample1);}
    }
  };
  
  //SERDES parameters
  SERDES_t  xcvrType;
  uint8_t   rxDataWidth;
  uint8_t   rxIntDataWidth;
  uint16_t  rxOutDiv;
  double    linkSpeedGbps;
  uint32_t  rxlpmen;

  // Scan parameters
  uint16_t maxXBinMag;
  uint16_t binXIncr;
  int16_t  binXBoundary;
  uint16_t binXCount;

  uint16_t maxYBinMag;
  uint16_t binYIncr;
  int16_t  binYBoundary;
  double   binYdV;
  
  bool link_up;
  
  ES_state_t es_state;
  DFE_state_t dfe_state;

  time_t startTime;
  time_t endTime;
  size_t  pixelsDone;

  // uint8_t prescale;
  uint8_t maxPrescale;

  //plotting variables
  float maxVoltage;
  float minVoltage;
  
  // access parameters
  std::string DRPBaseNode;
  std::string subDRPBaseNode;
  // double firstBER;
  std::vector<eyescanCoords> Coords_vect;
  std::vector<eyescanCoords>::iterator it;
  // std::vector<double> volt_vect;

public:
  eyescan() = delete;
  // Can find all nodes from DRP node
  eyescan(std::shared_ptr<BUTool::RegisterHelperIO> _IO, 
	  std::string const & _subDRPBaseNode, 
	  int _binXIncr, int _binYIncr, int _maxPrescale);
  ES_state_t check();
  //void check();
  void update();
  void reset();
  void restart();
  void throwException(std::string const &message);
  void fileDump(std::string const &outputFile); // Keep for BUTool?
  void writeCSV(std::string const &filename);
  void generateHeatmap(std::string const &filename, time_t global_start = 0);
  double getOpenArea();
  void setPrescale(int _maxPrescale);
  void setXIncr(int _xIncr);
  void setYIncr(int _yIncr);
  void operator+=(eyescan const &rscan);
  void swapIO(std::shared_ptr<BUTool::RegisterHelperIO> _IO);
  void kill();

  // Keep for BUTool?
  void GetProgress(size_t & _pixelCount, size_t & _pixelsDone){
    _pixelCount = Coords_vect.size();
    _pixelsDone = pixelsDone;
  };

  bool getLinkUp();

private:
  std::shared_ptr<BUTool::RegisterHelperIO> IO;
  void scan_pixel();
  void initialize();
  void realign();

  void EndPixelLPM();
  void EndPixelDFE();

  void SetEyeScanVoltage(std::string baseNode, uint8_t vertOffset, uint32_t sign);

  void SetEyeScanPhase(std::string baseNode, /*uint16_t*/ int horzOffset, uint32_t sign);

  std::vector<std::string> GenerateHeatmapTitle(std::string const & subDRPBaseNode, time_t global_start);
  
};


#endif
