#ifndef APOLLOSM_PROGRAM_FPGA_H
#define APOLLOSM_PROGRAM_FPGA_H

#ifdef __cplusplus
extern "C"
{
#endif

    int program_fpga(char *filename, bool displayProgress);

#ifdef __cplusplus
}
#endif

#endif