#include <ApolloSM/ApolloSM.hh>
#include <ApolloSM/eyescan_batch.hh>
#include <BUTool/ToolException.hh>
#include <algorithm>
#include <boost/filesystem/operations.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <memory>
#include <regex>
#include <standalone/daemon.hh>
#include <uhal/SigBusGuard.hpp>
#include <uhal/log/log_inserters.time.hpp>
#include <unistd.h>

#include <boost/program_options.hpp>

#include <sys/inotify.h>
#include <sys/select.h>
#include <sys/syslog.h>
#include <system_error>
#include <vector>

#include <boost/filesystem.hpp>

#define DEFAULT_PID_FILE "/var/run/eyescan_daemon.pid"
#define DEFAULT_RUN_DIR "/tmp"
#define DEFAULT_CONNECTIONS_FILE "/opt/address_table/connections.xml"
#define DEFAULT_OUTDIR "eyescans"
#define DEFAULT_HTML_DIR "/var/www/lighttpd/"

// #define WATCH_MASK (IN_CREATE | IN_MODIFY | IN_DELETE | IN_IGNORED)
#define WATCH_MASK (IN_DELETE | IN_CLOSE_WRITE | IN_ONLYDIR | IN_DONT_FOLLOW)
#define TARGET_SCAN_SEC 500
#define GLOBAL_QUALITY_THRESH 10 // Reset global scan if openArea is less than

size_t const inotifyEventSize = 2048;
uint8_t inotifyEvent[inotifyEventSize + 1];

struct inotify_event *readINotifyMessage(int fd) {
  // Assumes this has atleast one message in it
  struct inotify_event *event = (struct inotify_event *)inotifyEvent;
  memset(event, 0, inotifyEventSize);
  // get struct
  size_t readSize = read(fd, event, inotifyEventSize);
  if (readSize == size_t(-1)) {
    throw std::system_error(errno, std::generic_category(),
                            "inotify event read error");
  } else if (readSize < sizeof(struct inotify_event)) {
    throw std::system_error(errno, std::generic_category(),
                            "inotify event read size error");
  }
  return event;
}

class Prescale {
  /** A wrapper for the prescale to prevent multi-incrementing without
   * explicitly Specifying the end of the cycle has occurred
   */
private:
  bool m_unset = false;
  std::vector<std::string> killedNodes;

public:
  Prescale() {}
  Prescale(eyescan_batch *batch) { reload(batch); }
  void reset() { m_unset = false; }
  void unset() { m_unset = true; }
  void reload(eyescan_batch *batch) {
    killedNodes.clear();
    for (auto &node : batch->listBaseNodes()) {
      batch->changePrescale(node, 0);
    }
    m_unset = false;
  }
  void setUp(eyescan_batch *batch) {
    if (!m_unset) {
      auto k_end = killedNodes.end();
      auto upNodes = batch->linksUp();
      for (auto &node : batch->listBaseNodes()) {
        if (std::find(killedNodes.begin(), k_end, node) != k_end) {
          continue;
        }
        if (std::find(upNodes.begin(), upNodes.end(), node) == upNodes.end()) {
          continue;
        }
        if (batch->getPrescale(node) == 31) {
          syslog(LOG_INFO, "%s prescale at max value", node.c_str());
          continue;
        }
        batch->incrementPrescale(node);
        syslog(LOG_INFO, "%s prescale incremented to %d", node.c_str(),
               batch->getPrescale(node));
      }
    }
    m_unset = true;
  }
  void setDown(eyescan_batch *batch) {
    auto nodes = batch->terminateRunningScans();
    syslog(LOG_INFO, "Terminated %zu slow scans:", nodes.size());
    for (auto &node : nodes) {
      batch->decrementPrescale(node);
      syslog(LOG_INFO, "    %s", node.c_str());
    }
    killedNodes.insert(killedNodes.end(), nodes.begin(), nodes.end());
    m_unset = true;
  }
};

bool address_table_changed(int fd, std::string const &checkFilename) {
  bool changed = false;
  fd_set read_fd_set;
  FD_ZERO(&read_fd_set);
  FD_SET(fd, &read_fd_set);
  struct timespec timeout;
  timeout.tv_sec = 0;
  timeout.tv_nsec = 10000;
  int ret = pselect(fd + 1, &read_fd_set, NULL, NULL, &timeout, NULL);
  if (ret == -1) {
    // fd likely invalid... difficult to recover from at this point
    throw std::system_error(errno, std::generic_category(),
                            "pselect returned error");
  }

  while (ret > 0) {
    // read out the structs from inotify
    if (FD_ISSET(fd, &read_fd_set)) {
      // This is from the inotify watch
      struct inotify_event const *event = readINotifyMessage(fd);
      std::string filename(event->name, event->len);
      // fprintf(stderr,"Mask: 0x%08X File: %s\n",event->mask,filename.c_str());

      if (std::string::npos != filename.find(checkFilename)) {
        // This is a change in the filename we care about
        if (event->mask & IN_DELETE) {
          // the file was deleted, so a change is coming.  Wait until we get a
          // creation
          changed = true;
        } else if (event->mask & IN_CLOSE) {
          // we are done! (TODO wait for the file to be closed?)
          break;
        }
      } else {
        // A different file
        if (!changed) {
          // we haven't seen a delete for our checkFilename, so we can just
          // leave this loop otherwise we stay in this loop looking for the
          // create
          break;
        }
      }
    } else {
      // not our FD
      // fprintf(stderr,"Not my FD!\n");
      break;
    }

    // re-run pselect
    FD_ZERO(&read_fd_set);
    FD_SET(fd, &read_fd_set);
    timeout.tv_sec = 1;
    timeout.tv_nsec = 0;
    ret = pselect(fd + 1, &read_fd_set, NULL, NULL, &timeout, NULL);
    if (ret == -1) {
      // fd likely invalid... difficult to recover from at this point
      throw std::system_error(errno, std::generic_category(),
                              "pselect returned error");
    }
  }

  return changed;
}

bool scans_stable(eyescan_batch *batch) {
  for (auto &scan : batch->listBaseNodes()) {
    double open_area = batch->getOpenArea(scan);
    if (open_area == -1) {
      continue;
    } else if (open_area < GLOBAL_QUALITY_THRESH) {
      return false;
    }
  }
  return true;
}

void emitHTMLTable(FILE *fd, std::vector<std::string> const &nodes) {
  for (auto &node : nodes) {
    fprintf(fd, "<table border=\"1\" >\n");
    fprintf(fd, "  <tr>\n");
    fprintf(fd, "    <th colspan=\"2\">%s</th>\n", node.c_str());
    fprintf(fd, "  </tr>\n");
    fprintf(fd, "  <tr>\n");
    fprintf(fd, "    <td>Latest</td>\n");
    fprintf(fd, "    <td>Cumulative</td>\n");
    fprintf(fd, "  </tr>\n");
    fprintf(fd, "  <tr>\n");
    fprintf(fd, "    <td title=\"latest\"><img src=eyescans/%spng /></td>\n",
            node.c_str());
    fprintf(fd,
            "    <td title=\"cumulative\"><img src=eyescans/%scumulative.png "
            "/></td>\n",
            node.c_str());
    fprintf(fd, "  </tr>\n");
    fprintf(fd, "</table>\n");
  }
}

eyescan_batch::node_op_t runIntersectNodes(eyescan_batch *batch) {
  if (batch->intersectNodes() != eyescan_batch::node_op_t::OK) {
    for (auto const &error : batch->fetchErrorMessages()) {
      syslog(LOG_ERR, "Batch update returned error: %s: %s", error.what(),
             error.Description());
    }
    return eyescan_batch::node_op_t::ERROR;
  }
  return eyescan_batch::node_op_t::OK;
}

int main(int argc, char **argv) {
  // Opt parsing
  namespace po = boost::program_options;
  po::options_description desc("Start a daemon to produce eyescans");
  // clang-format off
  desc.add_options()
    ("help,h", "show this message and exit")
    ("pid-file,p", po::value<std::string>()->default_value(DEFAULT_PID_FILE),
     "process ID file")
    ("run-dir,d", po::value<std::string>()->default_value(DEFAULT_RUN_DIR),
     "directory to run daemon from")
    ("out-dir,o", po::value<std::string>()->default_value(DEFAULT_OUTDIR),
     "a subdirectory in html-dir to output scan data")
    ("no-daemon,n", po::bool_switch()->default_value(false),
     "allow the program to run as a normal program")
    ("connection-file,c", po::value<std::string>()->default_value(DEFAULT_CONNECTIONS_FILE),
     "connection file to use for apollosm")
    ("html-dir,l", po::value<std::string>()->default_value(DEFAULT_HTML_DIR),
     "directory to emit html eyescan display page");

  // clang-format on
  po::variables_map opts;
  po::store(po::parse_command_line(argc, argv, desc), opts);
  if (opts.count("help")) {
    std::cout << desc << "\n";
    return 0;
  }
  std::string const &pid_file = opts["pid-file"].as<std::string>();
  std::string const &run_dir = opts["run-dir"].as<std::string>();
  std::string const &html_dir = opts["html-dir"].as<std::string>();
  std::string const &out_dir =
      html_dir + "/" + opts["out-dir"].as<std::string>();
  std::string const &connection_file =
      opts["connection-file"].as<std::string>();

  // Make sure a directory exists to place eyescans
  try {
    boost::filesystem::path dir(out_dir.c_str());
    boost::filesystem::create_directory(dir);
    boost::filesystem::remove_all(dir); // Clears contents, but also dir itself
    boost::filesystem::create_directory(dir);
  } catch (std::exception const &e) {
    syslog(LOG_ERR, "Failed to create directory %s", out_dir.c_str());
    return 1;
  }

  boost::filesystem::path conn_path{connection_file};
  std::string watch_dir = conn_path.parent_path().string();
  std::string watch_file = conn_path.filename().string();

  // Boilerplate to run as daemon
  Daemon daemon;
  if (!opts["no-daemon"].as<bool>()) {
    daemon.daemonizeThisProgram(pid_file, run_dir);
  }
  struct sigaction sa_INT, sa_TERM, old_sa;
  daemon.changeSignal(&sa_INT, &old_sa, SIGINT);
  daemon.changeSignal(&sa_TERM, NULL, SIGTERM);
  //  daemon.changeSignal(&sa_TERM, NULL, SIGBUS);
  daemon.SetLoop(true);

  enum class state_t { INIT = 0, REINIT = 1, LOOP = 2, WAIT = 3, DONE = 4 };

  state_t state = state_t::INIT;
  int fdINotify = -1;
  Prescale prescale;
  std::vector<std::string> arg = {connection_file};
  std::shared_ptr<ApolloSM> SM;
  std::shared_ptr<ApolloSM> emptySM;
  eyescan_batch *batch = NULL;
  eyescan_batch::cycle_t ret;
  std::time_t batch_start;
  std::time_t batch_end;
  bool reloaded = false;
  // bool links_down = true;
  while (daemon.GetLoop()) {
    try {
      switch (state) {
      // ┌────────────────────────┐
      // │          INIT          │
      // │ Sets up inotify, batch │
      // │       SETS: LOOP       │
      // │                        │
      // └────────────────────────┘
      case state_t::INIT:
        // Initialize variables
        reloaded = false;
        // links_down = true;
        if (fdINotify >= 0) {
          close(fdINotify);
          fdINotify = -1;
        }
        // Setup inotify
        if ((fdINotify = inotify_init()) < 0) {
          throw std::system_error(errno, std::generic_category(),
                                  "inotify_init errored");
        }
        if (inotify_add_watch(fdINotify, watch_dir.c_str(), WATCH_MASK) == -1) {
          throw std::system_error(errno, std::generic_category(),
                                  "Cannot establish watch on " + watch_dir);
        }
        SM = std::make_shared<ApolloSM>(arg);
        batch = new eyescan_batch(SM, out_dir, 1, 1, 0);
        if (batch == NULL) {
          throw std::runtime_error("Failed to create new batch");
        }
        syslog(LOG_INFO, "Monitoring %s for deletion/creation of %s\n",
               watch_dir.c_str(), watch_file.c_str());

        syslog(LOG_INFO, "Batch created");
        runIntersectNodes(batch);
        std::time(&batch_start);

        {
          syslog(LOG_INFO, "Scanning\n");
          auto scanningNodes = batch->listBaseNodes();
          for (auto itNodes = scanningNodes.begin();
               itNodes != scanningNodes.end(); itNodes++) {
            syslog(LOG_INFO, "    %s\n", itNodes->c_str());
          }
        }

        state = state_t::LOOP;
        break;

        // ┌────────────────────────┐
        // │          LOOP          │
        // │    Runs scan updates   │
        // │    SETS: LOOP|REINIT   │
        // │          |WAIT|DONE    │
        // └────────────────────────┘

      case state_t::LOOP:
        if (address_table_changed(fdINotify, watch_file)) {
          syslog(LOG_INFO, "LOOP: address table changed\n");
          state = state_t::REINIT;
          break;
        }
        if (batch->listBaseNodes().size() == 0) {
          state = state_t::WAIT;
          break;
        }
        ret = batch->update();
        switch (ret) {
        case eyescan_batch::cycle_t::ERR_DONE:
          for (auto const &error : batch->fetchErrorMessages()) {
            syslog(LOG_ERR, "Batch update returned error: %s: %s", error.what(),
                   error.Description());
          }
          prescale.unset();
          /* fall through */
        case eyescan_batch::cycle_t::DONE:
          state = state_t::DONE;
          break;
        case eyescan_batch::cycle_t::ERR_RUNNING:
          for (auto const &error : batch->fetchErrorMessages()) {
            syslog(LOG_ERR, "Batch update returned error: %s: %s", error.what(),
                   error.Description());
          }
          break;
        case eyescan_batch::cycle_t::RUNNING:
          if (std::time(nullptr) - batch_start > TARGET_SCAN_SEC) {
            // Kills unfinished nodes and decrements their prescales
            batch->update(); // Puts it into done state for the next cycle
            prescale.setDown(batch);
          }
          break;
        case eyescan_batch::cycle_t::UNINIT:
          break;
        }
        break;
        // ┌────────────────────────┐
        // │          WAIT          │
        // │    Checks addr table   │
        // │    SETS: WAIT|REINIT   │
        // │                        │
        // └────────────────────────┘

      case state_t::WAIT:
        runIntersectNodes(batch);
        batch->update();             // Need to update to see if links are up
        batch->fetchErrorMessages(); // Clear any error messages
        if (batch->linksUp().size() != 0) {
          syslog(LOG_INFO, "WAIT: %zu links up", batch->linksUp().size());
          state = state_t::LOOP;
          break;
        }
        usleep(1e6);
        if (address_table_changed(fdINotify, watch_file)) {
          syslog(LOG_INFO, "WAIT: address table changed\n");
          state = state_t::REINIT;
          break;
        }
        break;
      // ┌────────────────────────┐
      // │         REINIT         │
      // │   Reloads addr table   │
      // │       SETS: LOOP       │
      // │                        │
      // └────────────────────────┘
      case state_t::REINIT:
        syslog(LOG_INFO, "Reloading address table");
        // Need something to check in order to sleep until fpga programmed
        batch->swapSM(emptySM);
        SM.reset();
        SM = std::make_shared<ApolloSM>(arg);
        syslog(LOG_INFO, "Created new ApolloSM");
        batch->swapSM(SM);
        runIntersectNodes(batch);
        {
          syslog(LOG_INFO, "Scanning\n");
          auto scanningNodes = batch->listBaseNodes();
          for (auto itNodes = scanningNodes.begin();
               itNodes != scanningNodes.end(); itNodes++) {
            syslog(LOG_INFO, "    %s\n", itNodes->c_str());
          }
        }
        prescale.reload(batch);
        std::time(&batch_start);

        reloaded = true;
        state = state_t::LOOP;
        break;
      // ┌────────────────────────┐
      // │          DONE          │
      // │  Emits html, restarts  │
      // │       SETS: LOOP       │
      // │                        │
      // └────────────────────────┘
      case state_t::DONE:
        state = state_t::LOOP; // If no condition changes it, next state is loop
        std::time(&batch_end);

        if (reloaded) {
          syslog(LOG_INFO, "Resetting accumulated scans after REINIT");
          batch->resetGlobalScans();
          reloaded = !scans_stable(batch);
          // Make sure prescale doesn't increment if links are not ready yet
          prescale.unset();
        }

        if (batch_end - batch_start < TARGET_SCAN_SEC) {
          prescale.setUp(batch);
        }
        // Emit html file to view plots
        FILE *ofd = fopen((html_dir + "/eyescans.html").c_str(), "w");
        if (ofd == NULL) {
          syslog(LOG_ERR, "Cannot open html file for writing");
        } else {
          fprintf(ofd, "<html>\n");
          fprintf(ofd, "<head><meta http-equiv=\"refresh\" content=\"30\" /> ");
          fprintf(ofd, "<style>\n");
          fprintf(ofd, "  table { float: left; margin: 10px;}\n");
          fprintf(ofd,
                  "  th { font-size: smaller; background-color:lightblue;}\n");
          fprintf(ofd, "  th.name {font-size: 20px; }\n");
          fprintf(ofd,
                  "  td { background-color:lightgrey; text-align: right;}\n");
          fprintf(ofd, "  td.nonerror { background-color:lightgreen;}\n");
          fprintf(ofd, "  td.warning { background-color:#FFFF00;}\n");
          fprintf(ofd, "  td.error { background-color:#FB412d;}\n");
          fprintf(ofd, "  td.null { background-color:lightgrey;}\n");
          fprintf(ofd, "</style>\n");
          fprintf(ofd, "</head>\n");
          fprintf(ofd, "<body>\n");
          emitHTMLTable(ofd, batch->listBaseNodes());
          fprintf(ofd, "</body>\n</html>");
          fclose(ofd);
        }

        prescale.reset();
        std::time(&batch_start);
        break;
      }
    } catch (BUException::exBase const &e) {
      syslog(LOG_ERR, "Caught BUException: %s\n   Info: %s (in state %d)",
             e.what(), e.Description(), int(state));
    } catch (std::system_error const &e) {
      syslog(LOG_ERR, "%s: %s  (in state %d)", e.what(),
             e.code().message().c_str(), int(state));
    } catch (std::exception const &e) {
      syslog(LOG_ERR, "Caught std::exception: %s  (in state %d)", e.what(),
             int(state));
    }
  }
  syslog(LOG_INFO, "Closing eyescan daemon");

  // Cleanup in case of exception
  if (batch != NULL) {
    delete batch;
  }
  sigaction(SIGINT, &old_sa, NULL);

  return 0;
}
