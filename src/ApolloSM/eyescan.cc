// clang-format off
//#include "RegisterHelper/RegisterHelperIO.hh"
//#include <ApolloSM/ApolloSM.hh>
#include <ApolloSM/eyescan_class.hh>
#include <ApolloSM/ApolloSM_Exceptions.hh>
#include <BUTool/ToolException.hh>

#include <IPBusIO/IPBusIO.hh>

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iterator>

#include <string>
#include <sstream>
#include <iomanip>

#include <utility>
#include <vector>
#include <queue>
#include <stdlib.h>
//#include <math.h> // pow
#include <syslog.h>
#include <time.h>
#include <inttypes.h> // for PRI macros
#include <algorithm>

#include <discpp.h>


#define WAIT 0x1
#define END 0x5
#define RUN 0x1
#define STOP_RUN 0x0
#define PRECISION 0.00000001 // 10^-9


std::vector<std::vector<std::string> > const static REQUIRED_TABLE_ELEMENTS ={ \
  {},									\
  {"ES_HORZ_OFFSET.REG","ES_HORZ_OFFSET.OFFSET","ES_HORZ_OFFSET.PHASE_UNIFICATION", \
   "RX_EYESCAN_VS.CODE","RX_EYESCAN_VS.REG","RX_EYESCAN_VS.SIGN","RX_EYESCAN_VS.UT", \
   "ES_EYE_SCAN_EN","ES_ERRDET_EN","ES_PRESCALE",			\
   "RX_DATA_WIDTH","RX_INT_DATAWIDTH",					\
   "ES_SDATA_MASK0","ES_SDATA_MASK1","ES_SDATA_MASK2",			\
   "ES_SDATA_MASK3","ES_SDATA_MASK4",					\
   "ES_QUALIFIER0","ES_QUALIFIER1","ES_QUALIFIER2",			\
   "ES_QUALIFIER3","ES_QUALIFIER4",					\
   "ES_QUAL_MASK0","ES_QUAL_MASK1","ES_QUAL_MASK2",			\
   "ES_QUAL_MASK3","ES_QUAL_MASK4",					\
   "RX_EYESCAN_VS.RANGE",						\
   "ES_CONTROL_STATUS",							\
   "ES_ERROR_COUNT","ES_SAMPLE_COUNT",					\
   "ES_CONTROL.RUN","ES_CONTROL.ARM",					\
   "RXOUT_DIV"								\
  },									\
  {"ES_HORZ_OFFSET.REG","ES_HORZ_OFFSET.OFFSET","ES_HORZ_OFFSET.PHASE_UNIFICATION",		\
   "ES_VERT_OFFSET.REG","ES_VERT_OFFSET.MAG","ES_VERT_OFFSET.SIGN","ES_VERT_OFFSET.UT",	\
   "ES_EYE_SCAN_EN","ES_ERRDET_EN","PMA_RSV2","ES_PRESCALE",		\
   "RX_DATA_WIDTH","RX_INT_DATAWIDTH",					\
   "ES_SDATA_MASK0","ES_SDATA_MASK1","ES_SDATA_MASK2",			\
   "ES_SDATA_MASK3","ES_SDATA_MASK4",					\
   "ES_QUALIFIER0","ES_QUALIFIER1","ES_QUALIFIER2",			\
   "ES_QUALIFIER3","ES_QUALIFIER4",					\
   "ES_QUAL_MASK0","ES_QUAL_MASK1","ES_QUAL_MASK2",			\
   "ES_QUAL_MASK3","ES_QUAL_MASK4",					\
   "ES_CONTROL_STATUS",							\
   "ES_ERROR_COUNT","ES_SAMPLE_COUNT",					\
   "ES_CONTROL.RUN","ES_CONTROL.ARM"					\
  },									\
  {"ES_HORZ_OFFSET.REG","ES_HORZ_OFFSET.OFFSET","ES_HORZ_OFFSET.PHASE_UNIFICATION",		\
   "RX_EYESCAN_VS.CODE","RX_EYESCAN_VS.REG","RX_EYESCAN_VS.SIGN","RX_EYESCAN_VS.UT",	\
   "ES_EYE_SCAN_EN","ES_ERRDET_EN","ES_PRESCALE",			\
   "RX_DATA_WIDTH","RX_INT_DATAWIDTH",					\
   "ES_SDATA_MASK0","ES_SDATA_MASK1","ES_SDATA_MASK2",			\
   "ES_SDATA_MASK3","ES_SDATA_MASK4","ES_SDATA_MASK5",			\
   "ES_SDATA_MASK6","ES_SDATA_MASK7","ES_SDATA_MASK8","ES_SDATA_MASK9",	\
   "ES_QUALIFIER0","ES_QUALIFIER1","ES_QUALIFIER2",			\
   "ES_QUALIFIER3","ES_QUALIFIER4","ES_QUALIFIER5",			\
   "ES_QUALIFIER6","ES_QUALIFIER7","ES_QUALIFIER8","ES_QUALIFIER9",	\
   "ES_QUAL_MASK0","ES_QUAL_MASK1","ES_QUAL_MASK2",			\
   "ES_QUAL_MASK3","ES_QUAL_MASK4","ES_QUAL_MASK5",			\
   "ES_QUAL_MASK6","ES_QUAL_MASK7","ES_QUAL_MASK8","ES_QUAL_MASK9",	\
   "ES_CONTROL_STATUS","RX_EYESCAN_VS.RANGE",				\
   "ES_ERROR_COUNT","ES_SAMPLE_COUNT",					\
   "ES_CONTROL.RUN","ES_CONTROL.ARM"					\
  },									\
  {"ES_HORZ_OFFSET.REG","ES_HORZ_OFFSET.OFFSET","ES_HORZ_OFFSET.PHASE_UNIFICATION",		\
   "ES_VERT_OFFSET.REG","ES_VERT_OFFSET.MAG","ES_VERT_OFFSET.SIGN","ES_VERT_OFFSET.UT",	\
   "ES_EYE_SCAN_EN","ES_ERRDET_EN","ES_PRESCALE",			\
   "RX_DATA_WIDTH","RX_INT_DATAWIDTH",					\
   "ES_SDATA_MASK0","ES_SDATA_MASK1","ES_SDATA_MASK2",			\
   "ES_SDATA_MASK3","ES_SDATA_MASK4",					\
   "ES_QUALIFIER0","ES_QUALIFIER1","ES_QUALIFIER2",			\
   "ES_QUALIFIER3","ES_QUALIFIER4",					\
   "ES_QUAL_MASK0","ES_QUAL_MASK1","ES_QUAL_MASK2",			\
   "ES_QUAL_MASK3","ES_QUAL_MASK4",					\
   "ES_CONTROL_STATUS",							\
   "ES_ERROR_COUNT","ES_SAMPLE_COUNT",					\
   "ES_CONTROL.RUN","ES_CONTROL.ARM"					\
  }									\
};


// RXOUT_DIV hex value (DRP encoding) vs max horizontal offset
// https://www.xilinx.com/support/documentation/application_notes/xapp1198-eye-scan.pdf pgs 8 and 9
#define MAX_HORIZONTAL_VALUE_MAG_SIZE 5
uint16_t const static MAX_HORIZONTAL_VALUE_MAG[MAX_HORIZONTAL_VALUE_MAG_SIZE] = {32,64,128,256,512};

#define GTH_USP_DELTA_V_SIZE 4
double const static GTH_USP_DELTA_V[GTH_USP_DELTA_V_SIZE] = {1.5,1.8,2.2,2.8}; //mv/count
#define GTY_USP_DELTA_V_SIZE 4
double const static GTY_USP_DELTA_V[GTY_USP_DELTA_V_SIZE] = {1.6,2.0,2.4,3.3}; //mv/count

// identifies bus data width
// read hex value (DRP encoding) vs bus width (attribute encoding)
#define BUS_WIDTH_SIZE 10
uint8_t const static BUS_WIDTH[BUS_WIDTH_SIZE] = {0,
						  0,
						  16,
						  20,
						  32,
						  40,
						  64,
						  80,
						  128,
						  160};

//Zero means an ivalid value
uint8_t const static INTERNAL_DATA_WIDTH[BUS_WIDTH_SIZE][3] = {{0,0,0},	\
							       {0,0,0}, \
							       {16,0,0}, \
							       {20,0,0}, \
							       {16,32,0}, \
							       {20,40,0}, \
							       {0,32,64}, \
							       {0,40,80}, \
							       {0,0,64}, \
							       {0,0,80}, \
};

// Does not need to be an ApolloSM function, only assertNode and confirmNode (below) will use this
void eyescan::throwException(std::string const &message) {
  BUException::EYESCAN_ERROR e;
  e.Append(message);
  throw e;
}

eyescan::eyescan(std::shared_ptr<BUTool::RegisterHelperIO> _IO, 
		 std::string const & _subDRPBaseNode, int _binXIncr,  //positive value
		 int _binYIncr, int _maxPrescale):
  es_state(UNINIT),
  IO(_IO){
  
  subDRPBaseNode=_subDRPBaseNode;
  // check for a '.' at the end of baseNode and add it if it isn't there 
  if(subDRPBaseNode.compare(subDRPBaseNode.size()-1,1,".") != 0) {
    subDRPBaseNode.append(".");
  }
  DRPBaseNode = subDRPBaseNode + "DRP.";
  
  //Set the node that tells us the LPM / DFE mode enabled
  if(!(IO->GetRegsRegex(subDRPBaseNode + "DEBUG.RX.LPM_EN").size())) {
    throwException("Cannot find LPM_EN at " + subDRPBaseNode +
        "DEBUG.RX.LPM_EN\n");
  }
  
  if(!(IO->GetRegsRegex(subDRPBaseNode + "DEBUG.EYESCAN_RESET").size())) {
    throwException("Cannot find EYESCAN_RESET at " + subDRPBaseNode +
        "DEBUG.EYESCAN_RESET\n");
  }
  
  if(!(IO->GetRegsRegex(subDRPBaseNode + "STATUS.PHY_LANE_UP").size())) {
    throwException("Cannot find EYESCAN_RESET at " + subDRPBaseNode +
        "STATUS.PHY_LANE_UP\n");
  }
  if(!(IO->GetRegsRegex(subDRPBaseNode + "STATUS.LINK_GOOD").size())) {
    throwException("Cannot find EYESCAN_RESET at " + subDRPBaseNode +
        "STATUS.LINK_GOOD\n");
  }
  
  //Determine the transceiver type
  if( (IO->GetRegsRegex(DRPBaseNode+"TYPE_7S_GTX")).size()){
    linkSpeedGbps = 5; //default to 30Gbps in 7s
    xcvrType=SERDES_t::GTX_7S;
  }else if( (IO->GetRegsRegex(DRPBaseNode+"TYPE_7S_GTH")).size()){
    linkSpeedGbps = 5; //default to 5 in 7s
    xcvrType=SERDES_t::GTH_7S;
  }else if( (IO->GetRegsRegex(DRPBaseNode+"TYPE_USP_GTH")).size()){
    xcvrType=SERDES_t::GTH_USP;
    linkSpeedGbps = 5; //default to 14 in USP GTH
  }else if( (IO->GetRegsRegex(DRPBaseNode+"TYPE_USP_GTY")).size()){
    xcvrType=SERDES_t::GTY_USP;
    linkSpeedGbps = 5; //default to 30Gbps in GTY
  }else{
    throwException("No transceiver type found for node "+DRPBaseNode+".\n");
  }

  //check that all needed addresses exist
  for(auto itCheck= REQUIRED_TABLE_ELEMENTS[int(xcvrType)].begin();
      itCheck != REQUIRED_TABLE_ELEMENTS[int(xcvrType)].end();
      itCheck++){
    if(!(IO->GetRegsRegex(DRPBaseNode+(*itCheck)).size())) {
      throwException("Missing "+DRPBaseNode+(*itCheck)+".\n");
    }
  }

  //Determine the prescale
  setPrescale(_maxPrescale);

  //Determine parameters for the horizontal axis
  rxOutDiv = IO->ReadRegister(DRPBaseNode + "RXOUT_DIV");
  if(rxOutDiv >= MAX_HORIZONTAL_VALUE_MAG_SIZE){
    throwException("RXOUT_DIV is outside of range.\n");
  }
  maxXBinMag = MAX_HORIZONTAL_VALUE_MAG[rxOutDiv];
  setXIncr(_binXIncr);

  //Determine the parameters for the vertical axis
  
  if(xcvrType == SERDES_t::GTX_7S || xcvrType == SERDES_t::GTH_7S){
    binYdV = -1;
  }else{
    uint8_t indexV = IO->ReadRegister(DRPBaseNode + "RX_EYESCAN_VS.RANGE");
    if(indexV >= 4){
      throwException("RX_EYESCAN_VX_RANGE invalid");
    }
    if (xcvrType == SERDES_t::GTH_USP){
      binYdV = GTH_USP_DELTA_V[indexV];
    }else if (xcvrType == SERDES_t::GTY_USP){
      binYdV = GTY_USP_DELTA_V[indexV];
    }else{
      binYdV = -1;
    }
  }
  maxYBinMag = MAX_Y_BIN_MAG;
  setYIncr(_binYIncr);

  
  
  // Check ES_EYE_SCAN_EN 
  if(IO->ReadRegister(DRPBaseNode+"ES_EYE_SCAN_EN") != 1){
    throwException("ES_EYE_SCAN_EN is not '1'.\n"
        "    Enabling it requires a PMA reset: " + subDRPBaseNode + "\n");
  }
  if( (xcvrType == SERDES_t::GTX_7S) && ((IO->ReadRegister(DRPBaseNode+"PMA_RSV2")&0x20) != 0x20)){
    throwException("PMA_RSV2 bit 5 must be '1' for GTX_7S SERDES.\n"
        "    Enabling it requires a PMA reset:" + subDRPBaseNode + "\n");
  }


  // Set ES_ERRDET_EN to 1 so that we have stastical mode, not waveform mode.
  IO->WriteRegister(DRPBaseNode + "ES_ERRDET_EN", 1);


  // ** ES_PRESCALE set prescale
  IO->WriteRegister(DRPBaseNode + "ES_PRESCALE", 0);

  
  //Get the values of RX_INT_DATAWIDTH and RX_DATA_WIDTH
  rxDataWidth = IO->ReadRegister(DRPBaseNode + "RX_DATA_WIDTH");
  if(rxDataWidth >= BUS_WIDTH_SIZE){
    throwException("RX_DATA_WIDTH is larger than 9\n");
  }
  rxIntDataWidth = IO->ReadRegister(DRPBaseNode + "RX_INT_DATAWIDTH");
  if(rxIntDataWidth >= 3){
    throwException("RX_INT_DATAWIDTH is larger than 2\n");
  }
  uint8_t internalDataWidth = INTERNAL_DATA_WIDTH[rxDataWidth][rxIntDataWidth];
  if(internalDataWidth == 0){
    throwException("RX_DATA_WIDTH(" + std::to_string(rxDataWidth) +
		   ") and RX_INT_DATAWIDTH(" + std::to_string(rxIntDataWidth) +
		   ") is an invalid combination\n.");
  }

  //Set ES bit masks
  //If in the future we want to only perform eyescans on specific special bit patterns,
  //Change Qalifier and QualMask appropriately
  int qualSize = (xcvrType == SERDES_t::GTY_USP || xcvrType == SERDES_t::GTH_USP ) ? 160  : 80;

  {
    uint16_t esQualifier, esQualMask, esSDataMask;
    esQualifier = esQualMask = esSDataMask = 0;
    //fill all of these multi-reg values    
    for(int iBit = 0; iBit <  qualSize; iBit++){
      //Move these down one bit
      esSDataMask >>= 0x1;
      esQualifier >>= 0x1;
      esQualMask  >>= 0x1;
      
      //Set the MSB of ex_X to a '1' if we want a 1 in this bit postion
      if(!(
	   (iBit < (qualSize / 2)) &&
	   (iBit >= (qualSize / 2 - internalDataWidth)))){
	//This is not a bit we care about, so mark it with a '1'
	esSDataMask |= 0x8000;
      }
      esQualMask |= 0x8000;
      //es_Qualifier we want to be '0', so we don't have to do anytying.

      if((iBit & 0xF) == 0xF){
	//we've filled 16 bits, let'w write it. 
	IO->WriteRegister(DRPBaseNode + "ES_SDATA_MASK"+
	    std::to_string(((iBit & 0xF0) >> 4)), esSDataMask);
	IO->WriteRegister(DRPBaseNode + "ES_QUALIFIER" +
	    std::to_string(((iBit & 0xF0) >> 4)), esQualifier);
	IO->WriteRegister(DRPBaseNode + "ES_QUAL_MASK" +
	    std::to_string(((iBit & 0xF0) >> 4)), esQualMask);
	//Shifts will remove old values
      }
    }
  }

  rxlpmen = IO->ReadRegister(subDRPBaseNode + "DEBUG.RX.LPM_EN");
  if(rxlpmen==DFE){
    dfe_state=FIRST;
  }else{
    dfe_state=LPM_MODE;
  }
  
  // If the link is down, we do not run the scan and indicate it in the results
  if (!getLinkUp()) {
    es_state = SCAN_DONE;
    return;
  }
  
  es_state=SCAN_INIT;
}

bool eyescan::getLinkUp() {
  link_up = true;
  if(!(IO->ReadRegister(subDRPBaseNode + "STATUS.PHY_LANE_UP"))) {
    link_up = false;
  }
  if(!(IO->ReadRegister(subDRPBaseNode + "STATUS.LINK_GOOD"))) {
    link_up = false;
  }
  return link_up;
}

eyescan::ES_state_t eyescan::check(){  //checks es_state
  return es_state;
}

void eyescan::swapIO(std::shared_ptr<BUTool::RegisterHelperIO> _IO) {
  IO.reset();
  IO = _IO;
  //  IO.swap(_IO);
}

void eyescan::restart() {
  if (es_state == SCAN_ERR || es_state == SCAN_READY || es_state == SCAN_INIT) {
    return;
  }
  if (es_state == UNINIT) {
    // Something went wrong in constructor
    return;
  }
  if (!link_up) {
    return;
  }
  if (es_state == SCAN_DONE || es_state == SCAN_KILL) {
    es_state = SCAN_INIT;
    if (dfe_state == SECOND) {
      dfe_state = FIRST;
    }
    return;
  }
  throwException(subDRPBaseNode + "scan in progress, cannot restart.\n");
}

void eyescan::setPrescale(int _maxPrescale) {
  if(_maxPrescale > MAX_PRESCALE_VALUE) {
    throwException("Prescale larger than max prescale.\n");
  } else {
    maxPrescale = _maxPrescale;
  }
}

void eyescan::setXIncr(int _xIncr) {
  if(_xIncr > maxXBinMag){
    throwException("XIncr is too large\n");
  }else if (_xIncr <=0){
    throwException("XIncr is too small\n");
  } else {
    binXIncr = _xIncr;
    binXBoundary = (maxXBinMag/binXIncr) * binXIncr;
    binXCount    = 2*(maxXBinMag/binXIncr) + 1;
  }
}

void eyescan::setYIncr(int _yIncr) {
  if(_yIncr > maxYBinMag){
    throwException("YIncr " + std::to_string(_yIncr) + " is too large (max " + 
                   std::to_string(maxYBinMag) + ")\n");
    //Compute the maximal horizontal x bin magnitude used for this scan
  }
  binYIncr = _yIncr;
  if(binYIncr == 0){
    binYBoundary = 0;
    //    binYCount    = 1;
  }else{
    binYBoundary = (maxYBinMag/binYIncr) * binYIncr;
    //    binYCount    = 2*(maxYBinMag/binYIncr) + 1;
  }
}

void eyescan::kill() {
  es_state = SCAN_KILL;
}

void eyescan::update(){
  switch (es_state){
  case SCAN_INIT:
    initialize();
    pixelsDone = 0;
    es_state = SCAN_READY;
    break;
  case SCAN_READY:
    time(&startTime);
    es_state = SCAN_START;
    break;
  case SCAN_START:
    scan_pixel();
    break;
  case SCAN_PIXEL:
    //0x5 = 0x1(done) | 0x2<<1 (END state)
    if (0x5 == IO->ReadRegister(DRPBaseNode + "ES_CONTROL_STATUS")){ 
      if (rxlpmen == DFE){
	EndPixelDFE();
      } else {
	EndPixelLPM();
      }
    }
    time(&endTime);
    break;
  case SCAN_DONE:
    // Must be a wait state
    break;
  case SCAN_KILL:
    fprintf(stderr,"SCAN_KILL\n");
    break;
  default :
    break;
  }
}

void eyescan::initialize(){
  // Figure out parameters common to all offsets outside of loop
  bool voltageReal;
  if(binYdV < 0) {
    voltageReal = false;
  } else {
    voltageReal = true;
  }
   
  // Set pixel at the data_sampler position (i.e. the origin) to be first in queue
  eyescan::eyescanCoords pixel;
  
  pixel.voltageReal = voltageReal;
  pixel.voltage = 0.0;
  pixel.phase = 0.0;
  pixel.vertWriteVal = 0;
  pixel.horzWriteVal = 0;
  if(xcvrType == SERDES_t::GTY_USP){
    // Phase unification bit in GTY depends on link speed
    if( linkSpeedGbps > 10){
      pixel.horzWriteVal |= 0x0800;
    }
  }
  Coords_vect.clear();
  Coords_vect.push_back(pixel);
  
  // Set the rest of the pixels
  for (int16_t iHorz   = -1*binXBoundary; iHorz <= binXBoundary; iHorz+=binXIncr){
    for (int16_t iVert = -1*binYBoundary; iVert <= binYBoundary; iVert+=binYIncr){
      pixel.reset();
      // Offset origin was put first in queue, so we don't need it again
      if(iHorz == 0 && iVert == 0) {
        continue;
      }
      
      //      eyescan::eyescanCoords pixel;      
      //Set this pixel up to be scanned.      

      //=====================================================
      // Floating point versions
      //verticle value
      pixel.voltageReal = voltageReal;
      if(voltageReal) {
	pixel.voltage=iVert*binYdV;
      } else {
	pixel.voltage=iVert;
      }
      
      //horizontal value
      pixel.phase = iHorz*(MAX_UI_MAG/maxXBinMag);

      //=====================================================
      //Register write versions
      //ES_VERT_OFFSET like value
      if(iVert >= 0){
	//set magnitude
	pixel.vertWriteVal = iVert & 0x007F;
	//Set offset sign and UT sign 
	pixel.vertWriteVal &= 0xFE7F;	
      }else{
	//set magnitude
	pixel.vertWriteVal = (-1*iVert) & 0x007F;
	//Set offset sign and UT sign 
	//	pixel.vertWriteVal |= 0x0180;	
	pixel.vertWriteVal |= 0x0080;	
      }
      
      //ES_HORZ_OFFSET like value
      pixel.horzWriteVal = iHorz&0x7FF;
      if(xcvrType == SERDES_t::GTY_USP){
	// Phase unification bit in GTY depends on link speed
	if( linkSpeedGbps > 10){
	  pixel.horzWriteVal |= 0x0800;
	}else{
	  //	  pixel.horzWriteVal &= 0xF7FF;
	  pixel.horzWriteVal &= 0x07FF;
	}
      }else{
	//all other transceivers
	//Set phase unification bit to '1' for all negative numbers
	if(iHorz < 0){	  
	  pixel.horzWriteVal |= 0x0800;
	}
      }
      Coords_vect.push_back(pixel);
    }
  }
  it=Coords_vect.begin();  
  es_state=SCAN_READY;
}

void eyescan::reset(){
  if (!link_up) {
    es_state = SCAN_DONE;
    return;
  }
  for (auto i = Coords_vect.begin(); i != Coords_vect.end(); ++i) {
    i->reset();
  }
  it = Coords_vect.begin();
  es_state=SCAN_READY;
}

void eyescan::realign() {
  // Realignment sequence from Xilinx documentation
  // Get the node below DRP
  IO->WriteRegister(DRPBaseNode + "ES_HORZ_OFFSET.REG", 0x880);
  IO->WriteAction(subDRPBaseNode + "DEBUG.EYESCAN_RESET");
  IO->WriteRegister(DRPBaseNode + "ES_HORZ_OFFSET.REG", 0x800);
  IO->WriteAction(subDRPBaseNode + "DEBUG.EYESCAN_RESET");
}

void eyescan::EndPixelLPM(){
  // read error and sample count
  
  uint32_t errorRawCount  = IO->ReadRegister(DRPBaseNode + "ES_ERROR_COUNT");
  uint32_t sampleRawCount = IO->ReadRegister(DRPBaseNode + "ES_SAMPLE_COUNT");

  uint64_t sampleCount;  
  
  IO->WriteRegister(DRPBaseNode + "ES_CONTROL.RUN",0);

  if(it == Coords_vect.begin() && errorRawCount != 0) {   
    realign();
    // Need to restart the scan after realignment
    es_state = SCAN_START;
    return;
  }

  //Update the sample count and error counts
  sampleCount = (1 << (1+(*it).prescale)) * sampleRawCount*BUS_WIDTH[rxDataWidth];
  (*it).error0 += errorRawCount;
  (*it).sample0 += sampleCount;


  // calculate BER
  (*it).BER = (*it).error0 / double((*it).sample0);
  if(((*it).BER < PRECISION) && ((*it).prescale != maxPrescale)) {
    (*it).prescale += PRESCALE_STEP; 
    if((*it).prescale > maxPrescale) {
      (*it).prescale = maxPrescale;         
    }      
    //keep scanning pixel to get BER
    es_state = SCAN_START;
  } else {
    if ((*it).error0==0){ //if scan found no errors default to BER floor
      (*it).BER = 1.0/double((*it).sample0);
    }
    (*it).sample1=0;
    (*it).error1=0;
    //move to the next pixel
    pixelsDone++;
    it++;
    if (it==Coords_vect.end()){
      es_state = SCAN_DONE;
    } else {
      es_state = SCAN_START;
    }
  }
  
//  //Make sure we get to the WAIT state
//  int while_count = 0;
//  while(IO->ReadRegister(DRPBaseNode+"ES_CONTROL_STATUS")!=0x1){
//    usleep(100);
//    if(while_count==10000){
//      throwException("EndPixelLPM: Stuck waiting for RUN=0 to move ES_CNOTROL_STATUS to state 0x0 (WAIT).");
//      break;
//    }
//    while_count++;
//  }
}

void eyescan::EndPixelDFE(){
  
  uint32_t errorRawCount  = IO->ReadRegister(DRPBaseNode + "ES_ERROR_COUNT");
  uint32_t sampleRawCount = IO->ReadRegister(DRPBaseNode + "ES_SAMPLE_COUNT");

  uint64_t sampleCount;
  IO->WriteRegister(DRPBaseNode + "ES_CONTROL.RUN",0);
  
  if(it == Coords_vect.begin() && errorRawCount != 0) {
    realign();
    // Need to restart the scan after realignment
    es_state = SCAN_START;
    return;
  }

  //Make sure we get out of the run state
  int while_count = 0;
  while(IO->ReadRegister(DRPBaseNode+"ES_CONTROL_STATUS")!=0x1){
    usleep(100);
    if(while_count==10000){
      throwException("EndPixelDFE: Stuck waiting for RUN=0 to move ES_CONTROL_STATUS to state 0x1. (WAIT)");
      break;
    }
    while_count++;
  }


  sampleCount = (1 << (1+(*it).prescale)) * sampleRawCount*BUS_WIDTH[rxDataWidth];
  if(dfe_state == FIRST){
    (*it).error0 += errorRawCount;
    (*it).sample0 += sampleCount;

    //keep scanning pixel to get BER
    //go to other side of DFE scan next time
    dfe_state = SECOND;
    es_state = SCAN_START;
  }else if (dfe_state == SECOND){
    (*it).error1 += errorRawCount;
    (*it).sample1 += sampleCount;
    //Compute total BER
    (*it).BER = ((*it).error0 / double((*it).sample0) +
		 (*it).error1 / double((*it).sample1));
    if(((*it).BER < PRECISION) && ((*it).prescale != maxPrescale)) {
      (*it).prescale += PRESCALE_STEP; 
      if((*it).prescale > maxPrescale) {
	(*it).prescale = maxPrescale;         
      }            
      //keep scanning pixel to get BER
      //go to other side of DFE scan next time
      dfe_state=FIRST;
      es_state = SCAN_START;
    } else {
      if ((*it).error1==0 && (*it).error0==0){ //if scan found no errors default to BER floor
	(*it).BER = 0.5/double((*it).sample0) + 0.5/double((*it).sample1);
      }
      //move to the next pixel
      it++;
      pixelsDone++;
      //go to other side of DFE scan next time
      dfe_state=FIRST;
      if (it==Coords_vect.end()){
        es_state = SCAN_DONE;
      } else {
	es_state = SCAN_START;
      }
    }
  }else{
    throwException("not in DFE mode");
  }
}


void eyescan::fileDump(std::string const &outputFile){

  std::vector<eyescan::eyescanCoords> const &esCoords = Coords_vect;
  
  FILE * dataFile = fopen(outputFile.c_str(), "w");
  if(dataFile!= NULL){
    
    for(size_t i = 0; i < esCoords.size(); i++) {
      fprintf(dataFile, "%0.9f ", esCoords[i].phase);
      if(esCoords[i].voltageReal){
	fprintf(dataFile, "%3.1f ", esCoords[i].voltage);
      }else{
	fprintf(dataFile, "%3.0f ", esCoords[i].voltage);
      }
      fprintf(dataFile, "%0.20f ", esCoords[i].BER);
      fprintf(dataFile, "%" PRIu64 " ", esCoords[i].sample0);
      fprintf(dataFile, "%" PRIu64 " ", esCoords[i].error0);
      fprintf(dataFile, "%" PRIu64 " ", esCoords[i].sample1);
      fprintf(dataFile, "%" PRIu64 " ", esCoords[i].error1);
      fprintf(dataFile, "%u\n", esCoords[i].prescale);
    }
    fclose(dataFile);
  } else {
    throwException("Unable to open file"+outputFile);
    //printf("Unable to open file %s\n", outputFile.c_str());
  }
}


void eyescan::scan_pixel(){
  //send the state back to WAIT
  IO->WriteRegister(DRPBaseNode + "ES_CONTROL.ARM", 0);
  IO->WriteRegister(DRPBaseNode + "ES_CONTROL.RUN", 0);
  int while_count = 0;
  while(IO->ReadRegister(DRPBaseNode+"ES_CONTROL_STATUS")!=0x1){   //done set in wait state
    usleep(100);
    if(while_count==10000){
      throwException("ScanPixel: Stuck waiting for run to reset.");
      break;
    }
    while_count++;
  }
  while_count=0;

  es_state = SCAN_PIXEL;

  //prescale
  IO->WriteRegister(DRPBaseNode + "ES_PRESCALE",(*it).prescale);
  
  //horizontal
  IO->WriteRegister(DRPBaseNode + "ES_HORZ_OFFSET.REG",(*it).horzWriteVal);

  //Vertical (special bitwise stuff is for the UT SIGN
  if(xcvrType == SERDES_t::GTH_USP || xcvrType == SERDES_t::GTY_USP){
    if(dfe_state == FIRST){
      IO->WriteRegister(DRPBaseNode + "RX_EYESCAN_VS.REG",(*it).vertWriteVal & 0xFF7F);
    }else if (dfe_state == SECOND){
      IO->WriteRegister(DRPBaseNode + "RX_EYESCAN_VS.REG",(*it).vertWriteVal | 0x0080);
    }else{
      IO->WriteRegister(DRPBaseNode + "RX_EYESCAN_VS.REG",(*it).vertWriteVal);
    }    
  }else if(xcvrType == SERDES_t::GTX_7S  || xcvrType == SERDES_t::GTH_7S ){
    if(dfe_state == FIRST){
      IO->WriteRegister(DRPBaseNode + "ES_VERT_OFFSET.REG",(*it).vertWriteVal & 0xFF7F);
    }else if (dfe_state == SECOND){
      IO->WriteRegister(DRPBaseNode + "ES_VERT_OFFSET.REG",(*it).vertWriteVal | 0x0080);
    }else{
      IO->WriteRegister(DRPBaseNode + "ES_VERT_OFFSET.REG",(*it).vertWriteVal);
    }    
  }

//  //send the state back to WAIT
//  IO->WriteRegister(DRPBaseNode + "ES_CONTROL.ARM", 0);
//  IO->WriteRegister(DRPBaseNode + "ES_CONTROL.RUN", 0);
//  int while_count = 0;
//  while(IO->ReadRegister(DRPBaseNode+"ES_CONTROL_STATUS")!=0x1){
//    usleep(100);
//    if(while_count==10000){
//      throwException("ScanPixel: Stuck waiting for run to reset.");
//      break;
//    }
//    while_count++;
//  }
//  while_count=0;

  //Start the next run and wait for us to move out of the wait state
  IO->WriteRegister(DRPBaseNode + "ES_CONTROL.RUN", 1);
  //Wait until the state is no lonter in WAIT or RESET (bits 4 or 3 are 0 in those states)
  while(! (IO->ReadRegister(DRPBaseNode+"ES_CONTROL_STATUS")&0xc) ){ 
    usleep(100);
    if(while_count==10000){
      throwException("ScanPixel: Stuck waiting for the run to start (get out of WAIT).");
      break;
    }
    while_count++;
  }
  while_count=0;
}

// Area of eyescan with no error in units of mV*UI
double eyescan::getOpenArea() {
  // Counts pixels in Coords_vect that have no error
  if(link_up) {
    int no_err_count = std::count_if(Coords_vect.begin(), Coords_vect.end(),
        [](eyescan::eyescanCoords coord) { return (coord.error0 == 0 && coord.error1 == 0); });
    
    double pixel_area = (binYIncr * binYdV) * (binXIncr * (MAX_UI_MAG / maxXBinMag));
    return no_err_count * pixel_area;
  } else {
    return -1;
  }
}

void eyescan::writeCSV(std::string const &filename) {
  FILE *fpointer = fopen(filename.c_str(), "w");
  if(fpointer == NULL) {
    throwException("Unable to open file " + filename + "\n");
  }
  
  // Write the Vivado-style header to file
  fprintf(fpointer, "SW Version,None\n");
  fprintf(fpointer, "GT Type,");
  if(link_up) {
    switch(xcvrType) {
      case SERDES_t::GTH_USP: fprintf(fpointer, "UltraScale GTH\n"); break;
      case SERDES_t::GTX_7S:  fprintf(fpointer, "7 Series GTX\n"); break;
      case SERDES_t::GTY_USP: fprintf(fpointer, "UltraScale GTY\n"); break;
      case SERDES_t::GTH_7S:  fprintf(fpointer, "7 Series GTH\n"); break;
      default:                fprintf(fpointer, "UNKNOWN\n"); break;
    }
  } else {
    fprintf(fpointer, "LINK DOWN\n");
    fprintf(fpointer, "Scan Name,%s\n", subDRPBaseNode.substr(0, subDRPBaseNode.size() - 1).c_str());
    if(fclose(fpointer) != 0) {
      throwException("Error closing file " + filename + "\n");
    }
    return;
  }
  fprintf(fpointer, "Date and Time Started,%s", asctime(localtime(&startTime)));
  fprintf(fpointer, "Date and Time Ended,%s", asctime(localtime(&endTime)));
  fprintf(fpointer, "Scan Name,%s\n", subDRPBaseNode.substr(0, subDRPBaseNode.size() - 1).c_str());
  double openArea = getOpenArea();
  fprintf(fpointer, "Open Area,%.1f\n", openArea);
  fprintf(fpointer, "Dwell,BER\n");
  fprintf(fpointer, "Dwell BER,%.1e\n", PRECISION); //Need to change to for 1e-8 for Vivado
  fprintf(fpointer, "Horizontal Increment,%d\n", binXIncr);
  fprintf(fpointer, "Horizontal Range,-0.500 UI to 0.500 UI\n");
  fprintf(fpointer, "Vertical Increment,%d\n", binYIncr);
  fprintf(fpointer, "Vertical Range,%.1f\n", binYdV); 
  fprintf(fpointer, "Scan Start\n");
  /////////////////////////////////////////////////////////////////////////////
  int blocksize = std::count_if(Coords_vect.begin(), Coords_vect.end(),
      [](eyescan::eyescanCoords a) { return a.phase == 0; });
  
  std::vector<eyescan::eyescanCoords *> coords_cp;
  coords_cp.reserve(Coords_vect.size());
  
  int count = 0;
  for(auto coord = Coords_vect.begin()+1; coord != Coords_vect.end(); ++coord) {
    if((*coord).phase == 0) {
      if(count == (blocksize - 1) / 2) {
        coords_cp.push_back(&(Coords_vect[0]));
      }
      ++count;
    }
    coords_cp.push_back(&(*coord));
  }

  
  fprintf(fpointer, "2d statistical");
  for(auto coordit = coords_cp.begin(); coordit < coords_cp.end(); coordit += blocksize) {
    fprintf(fpointer, ",%0.9f", (**coordit).phase);
  }
  fprintf(fpointer, "\n");
  for(int startindex = 0; startindex < blocksize; ++startindex) {
    if((*coords_cp[startindex]).voltageReal) {
      fprintf(fpointer, "%3.1f", (*coords_cp[startindex]).voltage);
    } else {
      fprintf(fpointer, "%3.0f", (*coords_cp[startindex]).voltage);
    }
    for(auto coordit = coords_cp.begin()+startindex; coordit < coords_cp.end();
        coordit += blocksize) {

      fprintf(fpointer, ",%0.20f", (**coordit).BER);
    }
    fprintf(fpointer, "\n");
  }

  fprintf(fpointer, "Scan End");
  if(fclose(fpointer) != 0) {
    throwException("Error closing file " + filename + "\n");
  }
}

std::vector<std::string> eyescan::GenerateHeatmapTitle(std::string const & subDRPBaseNode, time_t global_start){
  // Create character escapes in title
  std::vector<std::string> retStrings;
  std::stringstream title;
  std::string s = subDRPBaseNode.substr(0, subDRPBaseNode.size()-1);
  size_t pos = 0;
  while((pos = s.find("_")) != std::string::npos) {
    title << s.substr(0, pos);
    //    title += " ";
    s.erase(0, pos + 1);
  }
  pos = 0;
  while((pos = s.find(".")) != std::string::npos) {
    title << s.substr(0, pos);
    title << " ";
    s.erase(0, pos + 1);
  }
  title << s;


  if (!link_up) {
    title << " LINK DOWN";
  } else if (es_state == SCAN_KILL) {
    title << " SCAN TERMINATED";
  // TODO: add condition for unstable scans
  } else {
    title << " | ";
    time_t now;
    time(&now);
    char buffer[20];
    strftime(buffer,20,"%F %T",localtime(&now));
    title << buffer;
    //    title.str().pop_back();
    if (global_start != 0) {
      title << " | ";
      int diff_hrs = std::difftime(now, global_start) / 3600;
      if (diff_hrs < 24) {
        title << std::to_string(static_cast<int>(diff_hrs)); 
        title << "hrs";
      } else if (diff_hrs < 168) {
        title << std::to_string(static_cast<int>(diff_hrs / 24));
        title << "days";
      } else if (diff_hrs < 720) {
        title << std::to_string(static_cast<int>(diff_hrs / 168));
        title << "wks";
      } else {
        title << std::to_string(static_cast<int>(diff_hrs / 720));
        title << "mths";
      }
    }
    double open_area = getOpenArea();
    double total_area = Coords_vect.size() * (binYIncr * binYdV) *
                        (binXIncr * (MAX_UI_MAG / maxXBinMag));
    retStrings.push_back(""); //empty line 1
    retStrings.push_back(""); //empty line 2 
    retStrings.push_back(title.str()); //line 3
    title.str(std::string());
    title << "Open Area: " << std::setprecision(3) <<  open_area;
    title << " | Area Fraction: " << std::setprecision(3) << std::to_string(open_area / total_area);
    retStrings.push_back(title.str()); //line 4
    
  }
  return retStrings;
}

void eyescan::generateHeatmap(std::string const &filename, time_t global_start) {
  if(Coords_vect.empty()){
    return;
  }

  Dislin g;
  g.metafl("PNG"); //type
  g.filmod("DELETE");
  g.setfil(filename.c_str()); //finename
  g.scrmod("revers");  //background color to white
  g.disini(); //init
  g.hwfont();  //setup fonts (default ok?)
  g.triplx(); //set a better font
  
  bool noplot = false;
  if (!link_up || (es_state == SCAN_KILL)) {
    noplot = true;
  }
  
  //COmpute title
  std::vector<std::string> title = GenerateHeatmapTitle(subDRPBaseNode,global_start);
  for(size_t iT = 0; (iT < title.size()) && (iT < 4) ; iT++){
    g.titlin(title[iT].c_str(),iT + 1);  //Set title line iT+1 
  }
  g.name   ("UI Fraction","X");
  g.name   ("Voltage (mV)","Y");
  g.name   ("BER","Z");

  //create array
  std::vector<float> xVals;
  std::vector<float> yVals;
  std::vector<float> zVals;
  float minZ = 1;
  minVoltage = 0;
  maxVoltage = 0;
  for(auto coord = Coords_vect.begin()+1; coord != Coords_vect.end(); ++coord) {
    xVals.push_back(coord->phase);
    yVals.push_back(coord->voltage);
    if(!noplot){
      zVals.push_back(coord->BER);
      if(coord->BER < minZ){
	minZ = coord->BER;
      }      
    } else {
      zVals.push_back(1);
      minZ = 0.1;
    }

    if(coord->voltage < minVoltage){
      minVoltage = coord->voltage;
    }
    if(coord->voltage > maxVoltage){
      maxVoltage = coord->voltage;
    }
  }

  g.autres (2*binXBoundary+1,2*binYBoundary+1 ); //compute pixel sizes (n is number of datapoints in x,y)
  g.axspos (300, 1900);   //Set lower left corner of the plot
  g.ax3len (2200, 1600, 1600);  //set the lengths of the axes
  g.axsscl ("LOG","z"); //set logscale z
  g.labels ("LOG","z");
  
  g.graf3  (-0.5, 0.5, -0.5, 0.1,
	    minVoltage, maxVoltage, minVoltage, (maxVoltage-minVoltage)/5,
            floor(log10(minZ)), 0, floor(log10(minZ)), 1);
  
  
  g.curve3 (xVals.data(),yVals.data(),zVals.data(),zVals.size());
  
  g.height (50); //set the font height
  g.title  (); //plot the title
  g.disfin ();
}

void eyescan::operator+=(eyescan const &rscan) {
  auto coord = this->Coords_vect.begin();
  auto rcoord = rscan.Coords_vect.begin();

  //TODO check that the sizes are compatible 
  
  for (;
       coord != this->Coords_vect.end() && rcoord != rscan.Coords_vect.end();
       ++coord, ++rcoord) {
    coord->error0 += rcoord->error0;
    coord->error1 += rcoord->error1;
    coord->sample0 += rcoord->sample0;
    coord->sample1 += rcoord->sample1;
    switch(rxlpmen) {
      case DFE:
        coord->BER += coord->error0 / double(coord->sample0) +
                      coord->error1 / double(coord->sample1);
        if (coord->error0 == 0 && coord->error1 == 0) {
          coord->BER = 0.5 / double(coord->sample0) +
                       0.5 / double(coord->sample1);
        }
        break;
      case LPM:
        coord->BER = coord->error0 / double(coord->sample0);
        if (coord->error0 == 0) {
          coord->BER = 1.0 / double(coord->sample0);
        }
        break;
    }
  }
}
