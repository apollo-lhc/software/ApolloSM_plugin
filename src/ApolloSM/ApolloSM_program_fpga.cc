
#include <cstdlib> // For system()
#include <iostream>
#include <cstdio> // For popen(), fgets()
#include <string>

#include <ApolloSM/ApolloSM.hh>
#include <ApolloSM/ApolloSM_program_fpga.hh>

class ServiceManager
{
private:
  bool displayProgress; // Controls whether to print messages

public:
  // Constructor
  explicit ServiceManager(bool displayProgress) : displayProgress(displayProgress) {}

  // Check if a service is active
  bool isServiceActive(const std::string &serviceName)
  {
    std::string command = "systemctl is-active " + serviceName + " 2>&1";
    FILE *pipe = popen(command.c_str(), "r");
    if (!pipe)
    {
      if (displayProgress)
        std::cerr << "Error: Failed to check service status\n";
      return false;
    }

    char buffer[128];
    std::string result = "";
    while (fgets(buffer, sizeof(buffer), pipe) != nullptr)
    {
      result += buffer;
    }
    int status = pclose(pipe);
    if (status == -1)
    {
      if (displayProgress)
        std::cerr << "Error: Failed to close pipe\n";
      return false;
    }

    return (result.find("active") != std::string::npos);
  }

  // Stop the service
  bool stopService(const std::string &serviceName)
  {
    if (displayProgress)
      std::cout << "Stopping service: " << serviceName << std::endl;
    std::string command = "systemctl stop " + serviceName;
    int status = system(command.c_str());
    if (status != 0)
    {
      if (displayProgress)
        std::cerr << "Error: Failed to stop service " << serviceName << std::endl;
      return false;
    }
    return true;
  }

  // Start the service
  bool startService(const std::string &serviceName)
  {
    if (displayProgress)
      std::cout << "Starting service: " << serviceName << std::endl;
    std::string command = "systemctl start " + serviceName;
    int status = system(command.c_str());
    if (status != 0)
    {
      if (displayProgress)
        std::cerr << "Error: Failed to start service " << serviceName << std::endl;
      return false;
    }
    return true;
  }
};

int ApolloSM::program_fpga(std::string const &svfFile, bool displayProgress)
{
  std::vector<char> filename_copy(svfFile.begin(), svfFile.end());
  filename_copy.push_back('\0');

  ServiceManager manager(displayProgress);
  std::string serviceName = "xvc_cm1.service";
  bool startService = false;

  if (manager.isServiceActive(serviceName))
  {
    if (displayProgress)
      std::cout << "xvc_cm1.service is running." << std::endl;

    bool res = manager.stopService(serviceName);
    if (!res)
      return -1;
    startService = true;
  }
  else
  {
    if (displayProgress)
      std::cout << "xvc_cm1.service is not running, leaving it in this state." << std::endl;
  }

  int rc = ::program_fpga(filename_copy.data(), displayProgress); // Pass modifiable char* buffer

  if (startService)
  {
    manager.startService(serviceName);
  }

  return rc;
}
