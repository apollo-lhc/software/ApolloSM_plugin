/**
 * Copyright (C) 2021 Xilinx, Inc
 *
 * Modified by Aleksei Greshilov, 2022 CERN, aleksei.greshilov@cern.ch
 *
 * Licensed under the Apache License, Version 2.0 (the "License"). You may
 * not use this file except in compliance with the License. A copy of the
 * License is located at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/* DMA Proxy Test Application
 *
 * This application is intended to be used with the DMA Proxy device driver. It provides
 * an example application showing how to use the device driver to do user space DMA
 * operations.
 *
 * The driver allocates coherent memory which is non-cached in a s/w coherent system
 * or cached in a h/w coherent system.
 *
 * Transmit and receive buffers in that memory are mapped to user space such that the
 * application can send and receive data using DMA channels (transmit and receive).
 *
 * It has been tested with AXI DMA and AXI MCDMA systems with transmit looped back to
 * receive. Note that the receive channel of the AXI DMA throttles the transmit with
 * a loopback while this is not the case with AXI MCDMA.
 *
 * Build information: The pthread library is required for linking. Compiler optimization
 * makes a very big difference in performance with -O3 being good performance and
 * -O0 being very low performance.
 *
 * The user should tune the number of channels and channel names to match the device
 * tree.
 *
 * More complete documentation is contained in the device driver (dma-proxy.c).
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <stdint.h>
#include <signal.h>
#include <sched.h>
#include <time.h>
#include <errno.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h> // For C99 bool type

#include <ApolloSM/program_fpga.h>

/* The user must tune the application number of channels to match the proxy driver device tree
 * and the names of each channel must match the dma-names in the device tree for the proxy
 * driver node. The number of channels can be less than the number of names as the other
 * channels will just not be used in testing.
 */
#define TX_CHANNEL_COUNT 1
#define RX_CHANNEL_COUNT 1
#define MAX_SHIFT_BUF 4096

const char *tx_channel_names[] = {"dma_proxy_tx", /* add unique channel names here */};
const char *rx_channel_names[] = {"dma_proxy_rx", /* add unique channel names here */};

/* Internal data which should work without tuning */
struct channel
{
	struct channel_buffer *buf_ptr;
	int fd;
	pthread_t tid;
};

/*******************************************************************************************************************/
/* Get the clock time in usecs to allow performance testing
 */
static uint64_t get_posix_clock_time_usec()
{
	struct timespec ts;

	if (clock_gettime(CLOCK_MONOTONIC, &ts) == 0)
		return (uint64_t)(ts.tv_sec * 1000000 + ts.tv_nsec / 1000);
	else
		return 0;
}

unsigned int num_transfers;
long rest_size;
char *binfile;
struct stat st;
off_t binfile_size;

struct channel tx_channels[TX_CHANNEL_COUNT], rx_channels[RX_CHANNEL_COUNT];

int write_data(void *ch_ptr, bool displayProgress)
{
	unsigned int i, j, k, cntj = 0, buffer_id = 0;
	unsigned int nread = MAX_SHIFT_BUF;
	unsigned int cnt = 0;
	unsigned int buf[MAX_SHIFT_BUF / sizeof(unsigned int)] = {0};
	struct channel *channel_ptr = ch_ptr;
	int fd = open(binfile, O_RDONLY);
	if (fd < 0)
	{
		perror("ERROR opening DAT file");
		return -1;
	}

	if (stat(binfile, &st) < 0)
	{
		perror("ERROR in stat()");
		return -1;
	}
	else
	{
		binfile_size = st.st_size;
		if (displayProgress)
			printf("binfile size: %ld bytes\n", (long)binfile_size);
	}

	if (!(binfile_size - 8) % BUFFER_SIZE)
	{
		num_transfers = (binfile_size - 8) / BUFFER_SIZE + 1;
		rest_size = (binfile_size - 8) - (num_transfers - 1) * BUFFER_SIZE;
		if (displayProgress)
		{
			printf("num_transfers = %d\n", num_transfers);
			printf("rest_size = %ld\n", rest_size);
		}
		for (i = 0; i < num_transfers; i++)
		{
			/* Set up the length for the DMA transfer and initialize the transmit
			 * buffer to a known pattern.
			 */
			if (i % 20 == 0)
			{
				printf("\rProgress: %.2f%%", (100.0 * i) / num_transfers);
				fflush(stdout);
			}
			if (i == 0)
			{
				channel_ptr->buf_ptr[buffer_id].length = 8;

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				for (j = 0; j < 8 / sizeof(unsigned int); j++)
				{
					read(fd, &channel_ptr->buf_ptr[buffer_id].buffer[j], sizeof(unsigned int));
				}

				/* Restart the completed channel buffer to start another transfer and keep
				 * track of the number of transfers in progress
				 */
				ioctl(channel_ptr->fd, START_XFER, &buffer_id);

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				ioctl(channel_ptr->fd, FINISH_XFER, &buffer_id);
				if (channel_ptr->buf_ptr[buffer_id].status != PROXY_NO_ERROR)
				{
					printf("Proxy tx transfer error\n");
					return -1;
				}
			}
			if (i != 0)
			{
				cnt = 0;

				channel_ptr->buf_ptr[buffer_id].length = BUFFER_SIZE;

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				for (k = 0; k < BUFFER_SIZE / MAX_SHIFT_BUF; k++)
				{
					nread = read(fd, buf, MAX_SHIFT_BUF);

					for (j = 0; j < nread / sizeof(unsigned int); j++)
					{
						cntj = cnt / sizeof(unsigned int) + j;
						channel_ptr->buf_ptr[buffer_id].buffer[cntj] = buf[j];
					}

					// bzero(buf, nread);
					cnt += nread;
				}

				/* Restart the completed channel buffer to start another transfer and keep
				 * track of the number of transfers in progress
				 */
				ioctl(channel_ptr->fd, START_XFER, &buffer_id);

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				ioctl(channel_ptr->fd, FINISH_XFER, &buffer_id);
				if (channel_ptr->buf_ptr[buffer_id].status != PROXY_NO_ERROR)
				{
					printf("Proxy tx transfer error\n");
					return -1;
				}
			}
		}
	}
	else
	{

		num_transfers = (binfile_size - 8) / BUFFER_SIZE + 2;
		rest_size = (binfile_size - 8) - ((num_transfers - 2) * BUFFER_SIZE);
		if (displayProgress)
		{
			printf("num_transfers = %d\n", num_transfers);
			printf("rest_size = %ld\n", rest_size);
		}
		for (i = 0; i < num_transfers; i++)
		{
			if (i % 20 == 0)
			{
				printf("\rProgress: %.2f%%", (100.0 * i) / num_transfers);
				fflush(stdout);
			}
			/* Set up the length for the DMA transfer and initialize the transmit
			 * buffer to a known pattern.
			 */
			if (i == 0)
			{
				channel_ptr->buf_ptr[buffer_id].length = 8;

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				for (j = 0; j < 8 / sizeof(unsigned int); j++)
				{
					read(fd, &channel_ptr->buf_ptr[buffer_id].buffer[j], sizeof(unsigned int));
				}

				/* Restart the completed channel buffer to start another transfer and keep
				 * track of the number of transfers in progress
				 */
				ioctl(channel_ptr->fd, START_XFER, &buffer_id);

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				ioctl(channel_ptr->fd, FINISH_XFER, &buffer_id);
				if (channel_ptr->buf_ptr[buffer_id].status != PROXY_NO_ERROR)
				{
					printf("Proxy tx transfer error\n");
					return -1;
				}
			}

			if (i < num_transfers - 1 && i != 0)
			{
				cnt = 0;
				channel_ptr->buf_ptr[buffer_id].length = BUFFER_SIZE;

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				for (k = 0; k < BUFFER_SIZE / MAX_SHIFT_BUF; k++)
				{
					nread = read(fd, buf, MAX_SHIFT_BUF);

					for (j = 0; j < nread / sizeof(unsigned int); j++)
					{
						cntj = cnt / sizeof(unsigned int) + j;
						channel_ptr->buf_ptr[buffer_id].buffer[cntj] = buf[j];
					}
					cnt += nread;
				}

				/* Restart the completed channel buffer to start another transfer and keep
				 * track of the number of transfers in progress
				 */
				ioctl(channel_ptr->fd, START_XFER, &buffer_id);

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				ioctl(channel_ptr->fd, FINISH_XFER, &buffer_id);
				if (channel_ptr->buf_ptr[buffer_id].status != PROXY_NO_ERROR)
				{
					printf("Proxy tx transfer error\n");
					return -1;
				}
			}

			if (i == num_transfers - 1)
			{
				cnt = 0;
				channel_ptr->buf_ptr[buffer_id].length = rest_size;

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				for (k = 0; k < rest_size / MAX_SHIFT_BUF; k++)
				{
					nread = read(fd, buf, MAX_SHIFT_BUF);

					for (j = 0; j < nread / sizeof(unsigned int); j++)
					{
						cntj = cnt / sizeof(unsigned int) + j;
						channel_ptr->buf_ptr[buffer_id].buffer[cntj] = buf[j];
					}
					cnt += nread;
				}

				/* Restart the completed channel buffer to start another transfer and keep
				 * track of the number of transfers in progress
				 */
				ioctl(channel_ptr->fd, START_XFER, &buffer_id);

				/* Perform the DMA transfer and check the status after it completes
				 * as the call blocks til the transfer is done.
				 */
				ioctl(channel_ptr->fd, FINISH_XFER, &buffer_id);

				if (channel_ptr->buf_ptr[buffer_id].status != PROXY_NO_ERROR)
				{
					printf("Proxy tx transfer error\n");
					return -1;
				}
			}
		}
	}
	printf("\n");
	return 0;
}

int program_fpga(char *filename, bool displayProgress)
{
	int i;
	uint64_t start_time, end_time, time_diff;
	int mb_sec;
	int max_channel_count = MAX(TX_CHANNEL_COUNT, RX_CHANNEL_COUNT);

	if (displayProgress)
		printf("DMA proxy test\n");

	/* Open the file descriptors for each tx channel and map the kernel driver memory into user space */
	binfile = filename;
	if (displayProgress)
		printf("The filename is: %s\n", binfile); // Print the contents of the string

	for (i = 0; i < TX_CHANNEL_COUNT; i++)
	{
		char channel_name[64] = "/dev/";
		strcat(channel_name, tx_channel_names[i]);
		tx_channels[i].fd = open(channel_name, O_RDWR);
		if (tx_channels[i].fd < 1)
		{
			printf("Unable to open DMA proxy device file: %s\r", channel_name);
			// exit(EXIT_FAILURE);
			return -1;
		}
		tx_channels[i].buf_ptr = (struct channel_buffer *)mmap(NULL, sizeof(struct channel_buffer) * TX_BUFFER_COUNT,
															   PROT_READ | PROT_WRITE, MAP_SHARED, tx_channels[i].fd, 0);
		if (tx_channels[i].buf_ptr == MAP_FAILED)
		{
			printf("Failed to mmap tx channel\n");
			// exit(EXIT_FAILURE);
			return -1;
		}
	}

	start_time = get_posix_clock_time_usec();
	int rc = write_data((void *)&tx_channels[0], displayProgress);
	end_time = get_posix_clock_time_usec();
	time_diff = end_time - start_time;
	mb_sec = ((1000000 / (double)time_diff) * (((num_transfers - 2) * max_channel_count * (double)BUFFER_SIZE) + max_channel_count * (double)rest_size + max_channel_count * 8)) / 1000000;

	if (displayProgress)
	{
		printf("Time: %.2f%% seconds\n", (double)time_diff / 1000000.0);
		printf("Transfer size: %d Bytes\n", (((num_transfers - 2) * BUFFER_SIZE * max_channel_count) + (int)rest_size * max_channel_count + 8 * max_channel_count));
		printf("Throughput: %d MB/sec \n", mb_sec);
	}

	/* Clean up all the channels before leaving */
	for (i = 0; i < TX_CHANNEL_COUNT; i++)
	{
		munmap(tx_channels[i].buf_ptr, sizeof(struct channel_buffer));
		close(tx_channels[i].fd);
	}
	if (displayProgress)
		printf("DMA JTAG upload complete.\n");

	return rc;
}
