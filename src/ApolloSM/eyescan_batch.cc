#include "ApolloSM/ApolloSM.hh"
#include "ApolloSM/ApolloSM_Exceptions.hh"
#include "ApolloSM/eyescan_class.hh"
#include "BUException/ExceptionBase.hh"
#include "BUTool/ToolException.hh"
#include "RegisterHelper/RegisterHelperIO.hh"
#include <ApolloSM/eyescan_batch.hh>
#include <algorithm>
#include <cstdio>
#include <exception>
#include <iterator>
#include <memory>
#include <string>
#include <sys/syslog.h>
#include <uhal/log/log_inserters.time.hpp>
#include <utility>
#include <vector>

std::string const DRPLabel = "Scan";
using cycle_t = eyescan_batch::cycle_t;
using node_op_t = eyescan_batch::node_op_t;

eyescan_batch::eyescan_batch(std::shared_ptr<ApolloSM> _SM,
                             std::string const &outdir, int _xIncr = 4,
                             int _yIncr = 4, int _prescale = 0)
    : SM(_SM), defaultXIncr(_xIncr), defaultYIncr(_yIncr),
      defaultPrescale(_prescale) {

  odir = outdir;
  if (odir.compare(odir.size() - 1, 1, "/") != 0) {
    odir.append("/");
  }
}

node_op_t eyescan_batch::intersectNodes() {
  auto allNodes = discoverNodes();
  for (auto &node : allNodes) {
    if (node.compare(node.size() - 1, 1, ".") != 0) {
      node.append(".");
    }
    if (scanMap.count(node) == 0) {
      try {
        eyescan local(std::static_pointer_cast<BUTool::RegisterHelperIO>(SM),
                      node, defaultXIncr, defaultYIncr, defaultPrescale);
        eyescan global(std::static_pointer_cast<BUTool::RegisterHelperIO>(SM),
                       node, defaultXIncr, defaultYIncr, defaultPrescale);
        global.update();
        scanMap.emplace(
            std::make_pair(node, scanInfo(local, global, defaultXIncr,
                                          defaultYIncr, defaultPrescale)));
      } catch (BUException::EYESCAN_ERROR const &e) {
        scanMap.erase(node);
        exceptionMessages.push_back(e);
      } catch (BUException::BUS_ERROR const &e) {
        scanMap.erase(node);
        BUException::EYESCAN_ERROR err;
        err.Append("Caught SigBus error: ");
        err.Append(e.Description());
        exceptionMessages.push_back(err);
      }
    }
  }
  // Even if scanMap.size() <= discoverNodes().size(), might still be nodes to
  // be removed from scanMap since some discoverNodes() may not have been added
  // due to errors
  std::vector<std::string> oldNodes;
  for (auto &map_pair : scanMap) {
    if (std::find(allNodes.begin(), allNodes.end(), map_pair.first) ==
        allNodes.end()) {
      oldNodes.push_back(map_pair.first);
    } else {
      // If the node is good, chech the link state
      try {
        map_pair.second.link_up = map_pair.second.local.getLinkUp();
      } catch (BUException::EYESCAN_ERROR const &e) {
        // Remove problematic nodes as well
        oldNodes.push_back(map_pair.first);
        exceptionMessages.push_back(e);
      }
    }
  }
  for (auto node : oldNodes) {
    scanMap.erase(node);
  }
  if (exceptionMessages.size() != 0) {
    return node_op_t::ERROR;
  }
  return node_op_t::OK;
}

node_op_t eyescan_batch::addNodes(std::vector<std::string> _nodes, int _xIncr,
                                  int _yIncr, int _prescale) {
  int xIncr = (_xIncr == -1) ? defaultXIncr : _xIncr;
  int yIncr = (_yIncr == -1) ? defaultYIncr : _yIncr;
  int prescale = (_prescale == -1) ? defaultPrescale : _prescale;
  for (auto &node : _nodes) {
    if (node.compare(node.size() - 1, 1, ".") != 0) {
      node.append(".");
    }
    if (scanMap.count(node) == 1) {
      BUException::EYESCAN_ERROR e;
      e.Append("Cannot add " + node + " Node already exists.\n");
      exceptionMessages.push_back(e);
    }
    try {
      eyescan local(std::static_pointer_cast<BUTool::RegisterHelperIO>(SM),
                    node, xIncr, yIncr, prescale);
      eyescan global(std::static_pointer_cast<BUTool::RegisterHelperIO>(SM),
                     node, xIncr, yIncr, prescale);
      global.update();
      scanMap.emplace(std::make_pair(
          node, scanInfo(local, global, xIncr, yIncr, prescale)));
      scanMap.at(node).link_up = scanMap.at(node).local.getLinkUp();
    } catch (BUException::EYESCAN_ERROR const &e) {
      scanMap.erase(node);
      exceptionMessages.push_back(e);
    } catch (BUException::BUS_ERROR const &e) {
      scanMap.erase(node);
      BUException::EYESCAN_ERROR err;
      err.Append("Caught SigBus error: ");
      err.Append(e.Description());
      exceptionMessages.push_back(err);
    }
  }
  if (exceptionMessages.size() != 0) {
    return node_op_t::ERROR;
  }
  return node_op_t::OK;
}

std::vector<std::string> eyescan_batch::discoverNodes() {
  // All *.DRP to be scanned need a parameter="Scan=foo"
  std::vector<std::string> discoveredNodes;
  std::vector<std::string> nodes = SM->GetRegsRegex("*");
  for (std::string const &node : nodes) {
    // std::unordered_map<std::string, std::string> parameters
    auto parameters = SM->GetRegParameters(node);
    if (!parameters.empty() &&
        (parameters.find(DRPLabel) != parameters.end())) {
      discoveredNodes.push_back(node);
    }
  }
  return discoveredNodes;
}

cycle_t eyescan_batch::update() {
  // Effectively, "if all eyescans are in done state"
  // Or, if there are no loadedScans and completedScans == 0
  if (completedScans == (1 << scanMap.size()) - 1) {
    // Start a new scan!
    for (auto &map_pair : scanMap) {
      map_pair.second.local.reset();
      map_pair.second.link_up = map_pair.second.local.getLinkUp();
    }
    completedScans = 0;
  }

  int count = 0;
  for (auto &map_pair : scanMap) {
    try {
      map_pair.second.local.update();
      if (map_pair.second.local.check() == eyescan::SCAN_DONE ||
          map_pair.second.local.check() == eyescan::SCAN_KILL) {
        completedScans |= (1 << count);
      }
    } catch (BUException::BUS_ERROR const &e) {
      completedScans |= (1 << count);
      BUException::EYESCAN_ERROR err;
      err.Append("Caught SigBus error: ");
      err.Append(e.Description());
      exceptionMessages.push_back(err);
    }
    ++count;
  }
  if (completedScans == (1 << scanMap.size()) - 1) {
    // Generate plots
    plotScans();
    cycleStatus =
        (exceptionMessages.size() == 0) ? cycle_t::DONE : cycle_t::ERR_DONE;
  } else {
    cycleStatus = (exceptionMessages.size() == 0) ? cycle_t::RUNNING
                                                  : cycle_t::ERR_RUNNING;
  }
  return cycleStatus;
}

void eyescan_batch::plotScans() {
  for (auto &map_pair : scanMap) {
    try {
      map_pair.second.local.writeCSV(odir + map_pair.first + "csv");
      map_pair.second.local.generateHeatmap(odir + map_pair.first + "png");
      map_pair.second.global += map_pair.second.local;
      map_pair.second.global.writeCSV(odir + map_pair.first + "cumulative.csv");
      map_pair.second.global.generateHeatmap(
          odir + map_pair.first + "cumulative.png", map_pair.second.start);
    } catch (BUException::BUS_ERROR const &e) {
      BUException::EYESCAN_ERROR err;
      err.Append("Caught SigBus error: ");
      err.Append(e.Description());
      exceptionMessages.push_back(err);
    }
  }
}

std::vector<std::string> eyescan_batch::terminateRunningScans() {
  std::vector<std::string> killed;
  for (auto &map_pair : scanMap) {
    if (map_pair.second.local.check() != eyescan::SCAN_DONE) {
      map_pair.second.local.kill();
      map_pair.second.global.kill();
      killed.push_back(map_pair.first);
    }
  }
  completedScans = 0;
  return killed;
}

node_op_t eyescan_batch::changePrescale(std::string const &node,
                                        int _prescale) {
  if (cycleStatus != cycle_t::DONE) {
    return node_op_t::INVALID;
  }
  if (scanMap.count(node) == 0) {
    return node_op_t::ERROR;
  }
  try {
    scanMap.at(node).prescale = _prescale;
    scanMap.at(node).local.setPrescale(_prescale);
    scanMap.at(node).local.restart();
    scanMap.at(node).global.setPrescale(_prescale);
  } catch (BUException::EYESCAN_ERROR const &e) {
    exceptionMessages.push_back(e);
    return node_op_t::ERROR;
  }
  return node_op_t::OK;
}

node_op_t eyescan_batch::incrementPrescale(std::string const &node) {
  if (scanMap.at(node).prescale + 1 < 31) {
    return changePrescale(node, ++scanMap.at(node).prescale);
  } else {
    return node_op_t::INVALID;
  }
}

node_op_t eyescan_batch::decrementPrescale(std::string const &node) {
  if (scanMap.at(node).prescale - 1) {
    return changePrescale(node, --scanMap.at(node).prescale);
  } else {
    return node_op_t::INVALID;
  }
}

int eyescan_batch::getPrescale(std::string const &node) {
  return scanMap.at(node).prescale;
}

double eyescan_batch::getOpenArea(std::string node) {
  if (node.compare(node.size() - 1, 1, ".") != 0) {
    node.append(".");
  }
  if (cycleStatus != cycle_t::DONE) {
    return -1;
  }
  if (scanMap.count(node) == 0) {
    return -1;
  }
  // returns -1 if link is down
  return scanMap.at(node).local.getOpenArea();
}

std::vector<std::string> eyescan_batch::linksUp() {
  std::vector<std::string> links_up;
  for (auto map_pair : scanMap) {
    map_pair.second.link_up = map_pair.second.local.getLinkUp();
    if (map_pair.second.link_up) {
      links_up.push_back(map_pair.first);
    }
  }
  return links_up;
}

std::vector<BUException::EYESCAN_ERROR> eyescan_batch::fetchErrorMessages() {
  std::vector<BUException::EYESCAN_ERROR> exceptionMessagesCp =
      exceptionMessages;
  if (exceptionMessages.size() != 0) {
    exceptionMessages.clear();
  }
  return exceptionMessagesCp;
}

std::vector<std::string> eyescan_batch::listBaseNodes() {
  std::vector<std::string> nodes;
  for (auto &map_pair : scanMap) {
    nodes.push_back(map_pair.first);
  }
  return nodes;
}

void eyescan_batch::resetGlobalScans() {
  for (auto &map_pair : scanMap) {
    map_pair.second.global.reset();
    map_pair.second.start = std::time(nullptr);
  }
}

void eyescan_batch::swapSM(std::shared_ptr<ApolloSM> _SM) {
  for (auto &map_pair : scanMap) {
    map_pair.second.local.swapIO(
        std::static_pointer_cast<BUTool::RegisterHelperIO>(_SM));
    map_pair.second.local.reset();
    map_pair.second.global.swapIO(
        std::static_pointer_cast<BUTool::RegisterHelperIO>(_SM));
  }
  SM.reset();
  SM = _SM;
}
