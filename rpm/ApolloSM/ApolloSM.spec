#
# spefile for ApolloSM library
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: ApolloSM
License: Apache License
Group: ApolloSM
Source: https://gitlab.com/BUTool/ApolloSM_plugin
URL: https://gitlab.com/BUTool/ApolloSM_plugin
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
ApolloSM class for Apollo blade

%prep

%build

%install 

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
mkdir -p $RPM_BUILD_ROOT%{_python_prefix}
cp -rp %{sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.
cp -rp %{py_sources_dir}/lib/*  $RPM_BUILD_ROOT%{_python_prefix}/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/lib

%clean 

%post 

%postun 

%files 
%defattr(-, root, root)
%{_python_prefix}/ApolloSM.cpython-*.so
%{_prefix}/lib/libBUTool_ApolloSM.so
%{_prefix}/include/ApolloSM/*


