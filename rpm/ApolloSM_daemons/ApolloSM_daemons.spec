#
# spefile for ApolloSM daemons
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: ApolloSM_daemons
License: Apache License
Group: ApolloSM
Source: https://gitlab.com/BUTool/ApolloSM_plugin
URL: https://gitlab.com/BUTool/ApolloSM_plugin
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
ApolloSM daemons

%prep

%build

%install 

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
cp -rp %{sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/bin

%clean 

%post 

%postun 

%files 
%defattr(-, root, root)
%{_prefix}/include/standalone/*
%{_prefix}/bin/*


