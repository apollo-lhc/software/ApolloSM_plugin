#
# spefile for BUTool ApolloSM plugin
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: BUTool_ApolloSM_plugin
License: Apache License
Group: ApolloSM
Source: https://gitlab.com/BUTool/ApolloSM_plugin
URL: https://gitlab.com/BUTool/ApolloSM_plugin
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
ApolloSMDevice plugin for BUTool

%prep

%build

%install 

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
cp -rp %{sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/lib

%clean 

%post 

%postun 

%files 
%defattr(-, root, root)
%{_prefix}/lib/libBUTool_ApolloSMDevice.so
%{_prefix}/include/ApolloSM_device/*


